package LN;

import java.util.Comparator;

/**
 * Clase comparadora entre los precios de dos productos que implementa la 
 * interfaz comparator
 * @author Juan
 */
public class clsComparadorprecios implements Comparator<clsProducto> 
{
    /**
     * metodo que compara dos objetos
     * @param o1 objeto 1
     * @param o2 objeto 2
     * @return el objeto mayor
     */
    public int compare(clsProducto o1, clsProducto o2) {
        
        return ((Double)o1.getPrecio()).compareTo(o2.getPrecio());
    }
    
}
