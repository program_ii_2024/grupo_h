package LN;



/**
 * Clase que representa un accesorio.
 * Extiende la clase Producto.
 * 
 * @author Pablo
 */
public class clsAccesorio extends clsProducto
{
    /**
     * tipo de accesorio creado
     */
    private String strTipo;

    /**
     * Crea un nuevo accesorio con el nombre, precio y tipo especificados.
     *
     * @param strNombre El nombre del accesorio.
     * @param dblPrecio El precio del accesorio.
     * @param strTipo El tipo de accesorio (por ejemplo, "collar",
     * "reloj", etc.).
     */
    public clsAccesorio(String strNombre, double dblPrecio, String strTipo) 
    {
        super(strNombre, dblPrecio);
        this.strTipo = strTipo;
    }

    /**
     * constructor sin parametros de la clase accesorio
     */
    public clsAccesorio() 
    {
        super();
        this.strTipo = " ";
    }

    /**
     * Obtiene el tipo de accesorio.
     *
     * @return El tipo de accesorio.
     */
    public String getTipo() 
    {
        return strTipo;
    }

    /**
     * da valor al parametro strTipo
     * 
     * @param str_tipo el tipo a introducir
     */
    public void setTipo(String str_tipo) 
    {
        this.strTipo = str_tipo;
    }

    /**
     * metodo que implementa de itfproperty
     * @param propiedad la propiedad especificada
    * @return El valor actual de la propiedad especificada.
     */
    @Override
    public Object getProperty(String propiedad) {
        switch(propiedad)
		{
            case "tipo" : return this.strTipo;
            case "quesoy" : return "Accesorio";
            default:
            return super.getProperty(propiedad);
		}
		
		
    }


}
