SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mybd`;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Trabajador`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `mydb`.`Trabajador` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Trabajador` (
`Dni` varchar(64) not null,
`nombre` varchar(64) not null,
`apellido` varchar (64) not null,
`NNSS` int not null,
`contraseña` varchar(128) not null,
`FK_id_rol_trabajador` int not null,
primary key (`Dni`),
Foreign key (`FK_id_rol_trabajador`) references `mydb`.`rol_trabajador` (`id_rol_trabajador`)
ON DELETE NO ACTION ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table `mydb`.`rol_trabajador`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `mydb`.`rol_trabajador` ;

CREATE TABLE IF NOT EXISTS `mydb`.`rol_trabajador` (
`id_rol_trabajador` int not null auto_increment,
`descripcion_trabajador` varchar (64),
primary key (id_rol_trabajador));

-- -----------------------------------------------------
-- Table `mydb`.`Producto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Producto` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Producto` (
`nombre` varchar(64) not null,
`precio` double not null,
`talla_tipo` varchar(64) not null,
`oferta`  double not null,
`en_rol_Producto` ENUM('en_ROL_Prenda','en_ROL_Accesorio'	),
primary key (nombre));



-- -----------------------------------------------------
-- Table `mydb`.`Cliente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Cliente` ;
CREATE TABLE IF NOT EXISTS `mydb`.`Cliente` (
`Dni` VARCHAR(128) NOT NULL,
`Nombre` VARCHAR(128) NOT NULL,
`Apellido` VARCHAR(128) NOT NULL,
`Contraseña` VARCHAR(128) NOT NULL,
`Numero_de_Socio` int NOT NULL,
`puntos_de_socio` int NOT NULL,

en_rol_cliente ENUM('en_ROL_PREMIUM','en_ROL_SOCIONORMAL','en_ROL_INVALIDO'),
PRIMARY KEY (`Dni`));



- -----------------------------------------------------
-- Table `mydb`.`Rol_cliente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Rol_cliente` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Rol_cliente` (
`Id_cliente` INT NOT NULL AUTO_INCREMENT,
`Descripción cliente` VARCHAR(64) NOT NULL,
PRIMARY KEY (`Id_cliente`));

DROP TABLE IF EXISTS `mydb`.`Accesorios` ;
CREATE TABLE IF NOT EXISTS `mydb`.`Accesorios` (
`Nombre` VARCHAR(64) NOT NULL,
`Precio` VARCHAR(64) NOT NULL,
`Tipo` VARCHAR(64) NOT NULL,
PRIMARY KEY (`Nombre`));


insert into `mydb`.`cliente` (Nombre,Apellido,Dni,Contraseña,Numero_de_Socio,puntos_de_socio,en_rol_cliente)
 values ("prueba","prueba","00000","cliente",0,0,'en_ROL_SOCIONORMAL'),
 ("prueba","prueba","00001","cliente",1,0,'en_ROL_PREMIUM');

INSERT INTO `mydb`.`Producto` (`nombre`,`precio`,`talla_tipo`,`oferta`,`en_rol_Producto`) 
VALUES("prueba", 0, "L", 0, "en_ROL_Prenda");


describe cliente;

