package LN;




/**
 * Clase que representa una prenda de ropa.
 * Extiende la clase Producto.
 * @author Pablo
 */
public class clsRopa extends clsProducto

{
    /**
     * talla de la prenda
     */
    private String strTalla;
    /**
     * Constructor sin parametros para el objropa
     */
    public clsRopa()
    {
        this.strTalla = " ";
    }

    /**
     * Crea una nueva prenda de ropa con el nombre, precio y talla especificados.
     *
     * @param strNombre El nombre de la prenda de ropa.
     * @param dblPrecio El precio de la prenda de ropa.
     * @param strTalla  La talla de la prenda de ropa.
     */
    public clsRopa(String strNombre, double dblPrecio, String strTalla) 
    {
        super(strNombre, dblPrecio);
        this.strTalla = strTalla;
    }

    /**
     * Obtiene la talla de la prenda de ropa.
     * @return La talla de la prenda de ropa.
     */
    public String getTalla() 
    {
        return strTalla;
    }

    /**
     * da valor al atributo privado talla
     * @param str_talla talla de la prenda de ropa
     */
    public void setTalla(String str_talla)
    {
        this.strTalla = str_talla;
    }
    /**
     * metodo que implementa de la interfaz itfproperty de la clase ropa
     * @param propiedad el atributo a devolver
     * @return el atributo especificado en la propiedad
     */
    @Override
    public Object getProperty(String propiedad) {
        
        switch(propiedad)
		{
            case "talla" : return this.strTalla;
            case "quesoy" : return "Prenda de ropa";
            default:
            return super.getProperty(propiedad);
		}
		
		
    }

    
    


    
    

}


