package LP;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import javax.swing.table.DefaultTableModel;



import COMUN.clsConstantesLN;
import COMUN.itfProperty;
import Hilos.ClsUpdateTable;
import LN.clsGestorLN;
/**
 * formulario principal para manejar los productos implementa action listener
 * @author Juan
 */
public class frmPrincipalproducto extends 
JFrame implements ActionListener
{

    /**
     * panel en el que se contendran todos los objetos graficos
     */
    private JPanel Panel;
    /**
     * objeto gestor que se ha utilizado y contiene la informacion de los
     * diferentes productos
     */
    private clsGestorLN obj_Gestor;
    /**
     * tabla que contiene los productos
     */
    private JTable table;
    /**
     * text que contendra el nombre del producto
     */
    private JTextField txtNombre;
    /**
     * text que contendra el precio del producto
     */
    private JTextField txtPrecio;
    /**
     * text que contendra la oferta del producto
     */
    private JTextField txtOferta;
    /**
     * text que contendra la talla o el tipo del producto
     */
    private JTextField txtTalla_o_tipo;
    /**
     * modelo que se usa en la tabla
     */
    private DefaultTableModel model;
    /**
     * numero que contiene la fila que se ha tocado
     */
    private int row;
    /**
     * combo box que se usa para saber el si es accesorio o prenda de ropa
     */
    private JComboBox comboBox;

    /**
     * constructor del frmPrincipal
     * @param objGestor el objeto gestor que contiene toda la informacion de la 
     * LN
     */
    public frmPrincipalproducto(clsGestorLN objGestor)
    {
    
        obj_Gestor = objGestor;
        JFrame frame = new JFrame("Productos");
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(400, 180, 820, 500);
        Panel = new JPanel();
        Panel.setBounds(10, 10, 500, 200);

        
        
        Panel.setBackground(Color.green);
        setContentPane(Panel);
        Panel.setLayout(null);
        
        table = creacionTabla();
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setBounds(10, 10, 300, 400);
        Panel.add(scrollPane);

         txtNombre = new JTextField();
        txtNombre.setBounds(500, 58, 144, 20);
        txtNombre.setColumns(1);
        Panel.add(txtNombre);

       
        JLabel lblNewLabel = new JLabel("Nombre");
        lblNewLabel.setBounds(500, 42, 46, 14);
        Panel.add(lblNewLabel);


        txtPrecio = new JTextField();
        txtPrecio.setBounds(500, 98, 144, 20);
        Panel.add(txtPrecio);
        txtPrecio.setColumns(10);

        JLabel lblNewLabe2 = new JLabel("Precio");
        lblNewLabe2.setBounds(500, 82, 46, 14);
        Panel.add(lblNewLabe2);

 
        txtTalla_o_tipo = new JTextField();
        txtTalla_o_tipo.setBounds(500, 138, 144, 20);
        txtTalla_o_tipo.setColumns(2);
        Panel.add(txtTalla_o_tipo);

    
        JLabel lblNewLabe3 = new JLabel("Talla o tipo");
        lblNewLabe3.setBounds(500, 122, 100, 17);
        Panel.add(lblNewLabe3);


         txtOferta = new JTextField();
         txtOferta.setBounds(500, 178, 144, 20);
         txtOferta.setColumns(2);
         Panel.add(txtOferta);
 
   
         JLabel lblNewLabe4 = new JLabel("Oferta");
         lblNewLabe4.setBounds(500, 163, 46, 14);
         Panel.add(lblNewLabe4);


        
        JButton btnAceptar = new JButton("AÑADIR PRODUCTO");
        btnAceptar.addActionListener(this);
        btnAceptar.setActionCommand("A");
        btnAceptar.setBounds(500, 277, 180, 30);
        Panel.add(btnAceptar);

        JButton btnCancelar = new JButton("ELIMINAR");
        btnCancelar.addActionListener(this);
        btnCancelar.setActionCommand("E");
        btnCancelar.setBounds(500, 310, 180, 30);
        Panel.add(btnCancelar);

        JLabel lblNewLabe5 = new JLabel
        ("Para modificar, modifica directamente de la tabla");
         lblNewLabe5.setBounds(30, 380, 600, 100);
         Font fuente = new Font("Fantasy", Font.PLAIN, 20); 
        lblNewLabe5.setFont(fuente);
         Panel.add(lblNewLabe5);
        

        
        comboBox = new JComboBox <String> ();
		comboBox.setSize(225, 25);
		comboBox.setMaximumSize(new Dimension(225, 25));
        setItem();
		comboBox.setLocation(500, 230);
		comboBox.addActionListener(this);
		Panel.add(comboBox);
        ClsUpdateTable hilo = new ClsUpdateTable(this,objGestor);
        hilo.setDaemon(true);
        hilo.start();
    }



    /**
     * constructor de la tabla
     * @return la tabla creada
     */
    public JTable creacionTabla()
    {
        
        ArrayList<itfProperty> arr_productos;
        
        String[] columnNames = {"Nombre del producto",
                                "Precio",
                                "talla/tipo","oferta"};

        arr_productos = obj_Gestor.getarrayproductos("todos");
        int p = arr_productos.size();
            
        Object[][] datos;
        datos = new Object[p][4];

        for (int i = 0; i < datos.length; i++) 
        {
            itfProperty obj_producto = arr_productos.get(i);
            for (int j = 0; j < datos[i].length; j++) 
            {
                switch (j)
                 {
                    case 0:
                    datos[i][j] = obj_producto.getProperty
                    (clsConstantesLN.NOMBRE); 
                        break;
                    case 1:
                    datos[i][j] = obj_producto.getProperty
                    (clsConstantesLN.PRECIO);
                    break;
                    case 2:
                    if (obj_producto.getProperty(clsConstantesLN.ROL).
                    equals("Prenda de ropa")) 
                    {
                        datos[i][j] = obj_producto.getProperty
                        (clsConstantesLN.TALLA);

                    } else if (obj_producto.getProperty(clsConstantesLN.ROL).
                    equals("Accesorio"))
                    {
                        datos[i][j] = obj_producto.getProperty
                        (clsConstantesLN.TIPO); 
                    }
                    break;
                    case 3:
                    if ((double)
                    obj_producto.getProperty(clsConstantesLN.OFERTA) != 0)
                    datos[i][j] = obj_producto.getProperty
                    (clsConstantesLN.OFERTA);
                    else  datos[i][j] = 0;
                    break;

                    default:
                        break;
                }
            }
        }
        model = new DefaultTableModel(datos, columnNames);
        final JTable table = new JTable(model); //creacion de la tabla
        table.setPreferredScrollableViewportSize(new Dimension(600, 70));
        table.setFillsViewportHeight(false);
     
      
    
        table.addMouseListener(new MouseAdapter() {
            
                public void mouseClicked(MouseEvent e) {
                    añadirdatos(table);
                }
            });
        

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);

        //Add the scroll pane to this panel.
        add(scrollPane);
        return table;
    }



    /**
     * metodo que implementa de la interfac Actionlistener
     * @param e objeto que contiene la informacion del click del mouse
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        boolean valorRepetido;
            int numRows;
            String str_txt_nombre;
        switch (e.getActionCommand()) {
            case "A":
            valorRepetido = false;
            numRows = model.getRowCount();
            str_txt_nombre = (String) txtNombre.getText();
            for (int i = 0; i < numRows; i++) {
                String nombre = (String) model.getValueAt(i, 0);
                if(nombre.equals(txtNombre.getText())||  str_txt_nombre
                .equals(""))
                {
                    valorRepetido = true;
                }
            }
            if (!valorRepetido) {
                double numero =0;
                double oferta = 0;
                try {
                    numero = Double.parseDouble(txtPrecio.getText());
                    oferta = Double.parseDouble(txtOferta.getText());
                } catch (NumberFormatException i) {
                    System.out.println
                    ("No se pudo convertir el texto en un número double.");
                }
                String tipo = ((String) comboBox.getSelectedItem());
                System.out.println(tipo);
                switch (tipo)
                 {
                    case "Prenda ropa":
                        obj_Gestor.obj_altaRopa(txtNombre.getText(),
                        numero,txtTalla_o_tipo.getText(),oferta);
                        break;
                        
                    case "Accesorio":
                        obj_Gestor.obj_altaAccesorio(txtNombre.getText(),
                        numero,txtTalla_o_tipo.getText(),oferta);
                        
                    default:
                        System.out.println("Opcion incorrecta");
                        break;
                }
                if (txtOferta.getText().equals("0") || 
                txtOferta.getText().equals(null)) {
                    model.addRow(new Object[]{txtNombre.getText(),
                        numero,txtTalla_o_tipo.getText(),
                        0});  
                }else
                 model.addRow(new Object[]{txtNombre.getText(),
                    numero,txtTalla_o_tipo.getText(),
                    oferta});  
                
                
                
            
                    
                
            } else 
            {NombreRepetidoDialog dialog = new NombreRepetidoDialog(this);
            dialog.setVisible(true);}
           
                
                
                break;
            case "E":
            itfProperty obj_Producto;
            obj_Producto = obj_Gestor.obj_matchproducto
            (txtNombre.getText());
            obj_Gestor.bo_eliminarproducto(obj_Producto);
            model.removeRow(row);
            break;
            

        
            default:
                break;
        }
        
    }


    /**
     * funcion que añade en los diferentes cuadros de texto los datos 
     * selecciona dos en la tabla
     * @param table la tabla que contiene los datos
     */
    private void añadirdatos(JTable table) {
        
            
        row = table.getSelectedRow();
        javax.swing.table.TableModel model = table.getModel();

        String precio = String.valueOf(model.getValueAt(row, 1));
        String oferta = String.valueOf(model.getValueAt(row, 3));
        txtNombre.setText((String) model.getValueAt(row, 0));
        txtPrecio.setText(precio);
        txtTalla_o_tipo.setText((String) model.getValueAt(row,2));
        txtOferta.setText(oferta);
    }
    
    /**
     * añade los datos metidos en el cuadro de texto a una nueva fila en la 
     * tabla
     * @param nombre nombre del producto a añadir en la lista
     * @param numero precio del objeto
     * @param Talla_o_tipo añade la talla o el tipo del objeto
     */
    public void añadirproducto(String nombre, String numero, String Talla_o_tipo)
    {

        model.addRow(new Object[]{nombre,numero,txtTalla_o_tipo});
    }
    
    /**
     * añade las diferentes opciones al comboBox
     */
    @SuppressWarnings("unchecked")
    public void setItem()
	{
        this.comboBox.addItem("Elige...");
		this.comboBox.addItem("Prenda ropa");
        this.comboBox.addItem("Accesorio");
	}

    /**
     * devuelve el modelo creado
     * @return el modelo
     */
    public DefaultTableModel getTableModel()
    {
        return model;
    }
    

   

}