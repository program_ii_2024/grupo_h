package LN;


import COMUN.clsConstantesLN.en_ROL_CLIENTE;


/**
 * Clase que contiene atributos de un cliente. Es la clase hija de clsPersona.
 * @author Pablo
 */
public class clsCliente extends clsPersona
{
    /**
     * Numero de socio del cliente
     */
    private int int_Numsocio;
    /**
     * Rol del cliente
     */
    private en_ROL_CLIENTE en_rolcliente;
    /**
     * puntos de socio
     */
    private int int_puntosdesocio;

    /**
     * Obtiene el número de socio del cliente.
     * @return El número de socio asignado al cliente.
     */
    public int getNumsocio()
    {
        return this.int_Numsocio;
    }
   
    /**
     * Establece el número de socio del cliente.
     *
     * @param int_Numsocio El número de socio a establecer.
     */
    public void setNumsocio(int int_Numsocio)
    {
        this.int_Numsocio = int_Numsocio;
    }

    /**
     * Obtiene el rol del cliente.
     *
     * @return El rol del cliente (por ejemplo, "Cliente Regular", 
     * "Cliente Premium", etc.).
     */
    public en_ROL_CLIENTE getRolcliente()
    {
        return en_rolcliente;
    }

    /**
     * Establece el rol del cliente.
     *
     * @param en_Rolcliente El rol del cliente a establecer.
     */
    public void setRolcliente(en_ROL_CLIENTE en_Rolcliente) 
    {
        this.en_rolcliente = en_Rolcliente;
    }

     /**
     * Establece el número de puntos del cliente.
     * @param int_puntosdesocio son los puntos de socio que se van a añadir
     */
    public void set_puntosdesocio(int int_puntosdesocio) {
        this.int_puntosdesocio = int_puntosdesocio;
    }
    /**
     * Obtiene el número de puntos de socio del cliente.
     * @return devulve el valor de puntos de socios que tenga el cliente
     */
    public int get_puntosdesocio() {
        return int_puntosdesocio;
    }

/**
 * constructor con parametros del cliente
 * @param str_nombre nombre del cliente
 * @param str_Apellido apellido dle cliente
 * @param str_dni dni del cliente
 * @param str_contraseña contraseña del cliente
 * @param en_rolcliente rol del cliente
 * @param int_numSocio puntos de socio del cliente
 */
    public clsCliente(String str_nombre, String str_Apellido, String str_dni, 
    String str_contraseña, en_ROL_CLIENTE en_rolcliente, int int_numSocio) 
    {
        super(str_nombre, str_Apellido, str_dni, str_contraseña);
        this.int_Numsocio = int_numSocio; 
        this.en_rolcliente = en_rolcliente;
        this.int_puntosdesocio = 0;
        
    }
    /**
     * constructor sin parametros para la clase cliente
     */
    public clsCliente()
    {
        super();
        this.en_rolcliente=en_ROL_CLIENTE.en_ROL_INVALIDO;
        this.int_Numsocio = 0;
        this.int_puntosdesocio= 0;   
     }

/**
 * metodo que recibe de la implementacion de itfproperty
 * @param propiedad la propiedad especificada
 * @return El valor actual de la propiedad especificada.
 */
    public Object getProperty(String propiedad) {
        
        switch(propiedad)
		{
            case "Numero socio": return this.int_Numsocio;
            case "puntos de socio": return this.int_puntosdesocio;
            case "quesoy" : if 
            (this.en_rolcliente== en_ROL_CLIENTE.en_ROL_PREMIUM) 
            {
                return "socio premium"; 
            } else if (this.en_rolcliente== en_ROL_CLIENTE.en_ROL_SOCIONORMAL) {
                return "socio normal";
            }else return "cliente invalido";
            default:
            return super.getProperty(propiedad);
        }
        
    }
    

}
