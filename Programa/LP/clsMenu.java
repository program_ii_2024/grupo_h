package LP;
//actualizacion 1

import java.util.ArrayList;
import java.util.TreeSet;

import javax.swing.SwingUtilities;

import COMUN.clsConstantesLN;
import COMUN.itfProperty;
import Excepciones.clsPersonaRepetidaExcepcion;
import COMUN.clsConstantesLN.en_ROL_CLIENTE;
import COMUN.clsConstantesLN.en_ROL_TRABAJADOR;
import LN.clsGestorLN;


/**
 * Clase que maneja todos los menus que va a ver el usuario
 * @author Juan
 */
public class clsMenu 
{
    /**
     * objeto gestor que sirve para llamar a los metodos del gestor
     */
    private LN.clsGestorLN objGestor;

    /**
     * constructor de la clase menu
     */
    public clsMenu()
    {
        objGestor = new clsGestorLN();
    }
    
    /**
     * funcion que comienza con el menu inicializando unos objetos de ejemplo
     * , descargando datos de la base de datos y metiendolos al programa, por
     * ultimo llama al menu
     */
    public void vo_comienzomenu()
    {
        try {
            objGestor.vo_inicializacionobjetos();
        } catch (clsPersonaRepetidaExcepcion e) {
            System.out.println(e);
        }
        
        objGestor.arr_inicializararrays();
        vo_menuPrincipal();
    } 
    /**
     * Menu principal que te lleva al menu de trabajador o al de cliente
     */
    public void vo_menuPrincipal()
    {
        
        int int_opcion;

        int_opcion = 99;
        do 
        {
            System.out.println("1- Menu de trabajador");
            System.out.println("2- Menu de cliente.");
            System.out.println("0- Salir.");
            System.out.println("Introduce una opcion:");
            int_opcion = clsUtilidadesLP.leerEntero();
            switch (int_opcion) 
            {
                case 1:
                       vo_Menutrabajador();
                    break;
                case 2:
                vo_MenuCliente();
                    break;
                case 3:
           
                    break;
                case 0:
                    System.out.println("Has cerrado el programa.");
                    break;
                default:
                    System.out.println("Opcion no valida.");
                    break;
            }
        } while (int_opcion != 0);
    }

    /**
     * menu principal del trabajador que sirve de puerta de entrada para
     * el siguiente menu dependiendo de que rol de trabajador tienes
     */
    public void vo_Menutrabajador()
    {
        int int_opcion;
        itfProperty obj_trabajador;
        
        int_opcion = 99;
        System.out.println("1- Inicio sesión");
        System.out.println("2- Nuevo trabajador");
        int_opcion=clsUtilidadesLP.leerEntero();
        switch (int_opcion) 
        {
            case 1:
                obj_trabajador = obj_menumatchtrabajador();
                vo_selectormenustrabajador(obj_trabajador);
                break;
            case 2:
            obj_trabajador = obj_menumatchtrabajador();
                
                vo_selectormenustrabajador(obj_trabajador);
                break;
        
            default:
                break;
        }

    }

    /**
     * Comprueba si el dni y contraseña que añades por pantalla son validos 
     * entre los trabajadores ya registrados
     * @return devuelve el obj_trabajador del usuario
     */
    public itfProperty obj_menumatchtrabajador()
    {
        
        String str_DNI;
        String str_contraseña;
        int int_seguir = 1;
    do 
    {
        System.out.println("Añade DNI");
        str_DNI = clsUtilidadesLP.leerCadena();
        System.out.println("Añade contraseña");
        str_contraseña = clsUtilidadesLP.leerCadena();
        if (!objGestor.bo_ComprobacionTrabajador(str_DNI, str_contraseña))
        {       
            System.out.println("DNI o contraseña no validos");
    
        } else int_seguir = 2;
        
      } while ( int_seguir == 1);
      
      return objGestor.obj_matchTrabajador(str_DNI);
    }

 

    /**
     * Añade un trabajador nuevo
     * @return devuelve el obj creado del trabajador
     */
    public itfProperty obj_Menualtatrabajador()
    {
        String str_nombre;
        String str_apellido;
        String str_DNI;
        String str_contraseña1;
        String str_contraseña2;
        int int_NNSS;
        String str_rol;
        en_ROL_TRABAJADOR en_rolTrabajador;
        System.out.println("Introduce nombre");
        str_nombre=clsUtilidadesLP.leerCadena();
        System.out.println("Introduce apellido");
        str_apellido= clsUtilidadesLP.leerCadena();
        System.out.println("Introduce dni");
        str_DNI= clsUtilidadesLP.leerCadena();
        do
        {
            System.out.println("Introduce contraseña");
            str_contraseña1= clsUtilidadesLP.leerCadena();
            System.out.println("vuelve a introducir contraseña");
            str_contraseña2= clsUtilidadesLP.leerCadena();
            if (!str_contraseña1.equals(str_contraseña2)) 
            {
                System.out.println("no has introducido la misma contraseña");
            }
        }
        while (!str_contraseña1.equals(str_contraseña2));
        System.out.println("introduce numero de la seguridad social");
        int_NNSS =clsUtilidadesLP.leerEntero();
        System.out.println("introduce rol A|D");
        str_rol=clsUtilidadesLP.leerCadena();
        switch (str_rol) 
        {
            case "A":
                en_rolTrabajador = COMUN.clsConstantesLN.
                en_ROL_TRABAJADOR.en_ROL_ALMACENERO;
                break;
            case "D" :
                en_rolTrabajador = COMUN.clsConstantesLN.
                en_ROL_TRABAJADOR.en_ROL_DEPENDIENTE;
                break;
            default:
                en_rolTrabajador = COMUN.clsConstantesLN.
                en_ROL_TRABAJADOR.en_ROL_INVALIDO;
                break;

        }
    
        try {
            return objGestor.vo_altaTrabajador(str_nombre, str_apellido,
             str_DNI, 
            str_contraseña1, int_NNSS, en_rolTrabajador);
        } catch (clsPersonaRepetidaExcepcion e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * sirve para llevarte a los diferentes menus que hay dependiendo de tu rol
     * como trabajador
     * @param objTrabajador recibe el objeto trabajador para ver cual es su rol
     * 
     */
    public void vo_selectormenustrabajador(itfProperty objTrabajador)
    {
        String rol;
        rol = (String) objTrabajador.getProperty
        (clsConstantesLN.ROL);
        switch (rol)
        {
        
            case "dependiente": 
            vo_menudependiente();
            break;

            case "almacenero":
            vo_menuAlmacenero();
            break;

            default:
            System.out.println("rol invalido");
            break;               
        }
    }

    /**
     * Menu del dependiente
     * 
     */
    public void vo_menudependiente()
    {
        
        int int_opcion = 99;
        
        System.out.println("Elige una opcion:");
        System.out.println("1.-Crear cliente.");
        System.out.println("2.-baja cliente");
        System.out.println("3.-modificacion cliente");
        System.out.println("4.-Realizar cobro en caja.");
        System.out.println("5.-listar clientes");
        System.out.println("6.-añadir puntos cliente");
        System.out.println("7.-Salir.");
        int_opcion = clsUtilidadesLP.leerEntero();
        switch (int_opcion) {
            case 1:
                vo_nuevoCliente();
                break;

            case 2:
                vo_bajacliente(true);
            case 3:
                vo_modificarcliente();
            case 4:
                vo_cobrarCaja();
                break;
            case 5: 
                vo_listarclientes();
                break;
            case 6:
                vo_añadirpuntoscliente();
            case 7:
                vo_menuPrincipal();
            default:
                break;
        }
    }
    /**
     * funcion mas de ejemplo que te permite añadir puntos de cliente para 
     * ver como va el update a la base de datos
     */
    public void vo_añadirpuntoscliente()
    {
        String str_dni;
        double dbl_precio;
        System.out.println("añade dni");
        str_dni = clsUtilidadesLP.leerCadena();
        System.out.println("añade puntos de socio a sumar");
        dbl_precio= clsUtilidadesLP.leerEntero();
        objGestor.vo_sumapuntoscliente(dbl_precio, str_dni);
        vo_menudependiente();
    }
    /**
     * funcion que dice los diferentes clietnes que hay en la base de datos
     */
    public void vo_listarclientes()
    {
        ArrayList<itfProperty> retorno;
        retorno = objGestor.ListarCLientesBD();
        for (itfProperty obj_cliente : retorno) 
        {
            System.out.print(obj_cliente.getProperty
            (clsConstantesLN.NOMBRE)+ " ");
            System.out.print(obj_cliente.getProperty
            (clsConstantesLN.APELLIDO)+ " ");
            System.out.print(obj_cliente.getProperty
            (clsConstantesLN.DNI )+ " ");
            System.out.print(obj_cliente.getProperty
            (clsConstantesLN.CONTRASEÑA)+ " ");
            System.out.print(obj_cliente.getProperty
            (clsConstantesLN.NUM_SOCIO)+ " ");
            System.out.print(obj_cliente.getProperty
            (clsConstantesLN.PUNTOS_DE_SOCIO) + " ");
            System.out.println
            (obj_cliente.getProperty(clsConstantesLN.ROL) + " ");
        

        }
        vo_menudependiente();
    }

    /**
     * funcion que de de baja a un cliente en la base de datos
     * @param volver sirve para saber si tiene que volver al menu o ha sido 
     * llamada por otra funcion que no era el menudependiente
     */
    public void vo_bajacliente(boolean volver)
    {
        String str_Dni;
        System.out.println("añade dni del cliente");
        str_Dni = clsUtilidadesLP.leerCadena();
        objGestor.vo_bajacliente(str_Dni);
        if (volver) {
            vo_menudependiente();
        }
    }
    /**
     * funcion que mofidica a un cliente por completo
     */
    public void vo_modificarcliente()
    {
        vo_bajacliente(false);
        vo_nuevoCliente();
    }
    /**
     * Este metodo se encarga de dar del alta socios.
     */
    public void vo_nuevoCliente()
    {
        String str_nombre;
        String str_apellido;
        String str_DNI;
        String str_contraseña;
        int int_numSocio;

        System.out.println("Añadir nombre del cliente:");
        str_nombre = clsUtilidadesLP.leerCadena();
        System.out.println("Añadir apellido del cliente:");
        str_apellido = clsUtilidadesLP.leerCadena();
        System.out.println("Añadir dni del cliente:");
        str_DNI = clsUtilidadesLP.leerCadena();
        System.out.println("Añadir contraseña del cliente:");
        str_contraseña = clsUtilidadesLP.leerCadena();
        System.out.println("Añadir numero de socio");
        int_numSocio = clsUtilidadesLP.leerEntero();

        try {
            objGestor.obj_nuevoCliente(str_nombre, str_apellido, str_DNI, 
            str_contraseña,int_numSocio);
        } catch (clsPersonaRepetidaExcepcion e) {
            System.out.println(e);
        }
        vo_menudependiente();

    }

    
    /**
     * Pide los datos para hacer un cobro en caja y para añadir puntos al socio
     * que ha hecho la compra
     */
    public void vo_cobrarCaja()
    {
        char a;
        String str_nombreproducto;
        String str_dni;
        itfProperty obj_Producto;
        double dbl_precio;
        double dbl_total;
        dbl_precio = 0;
        dbl_total = 0;
        vo_mostrarlistaproductos(1,false);
        System.out.println("añade dni");
        str_dni = clsUtilidadesLP.leerCadena();

        do {
            System.out.println("Que producto ha pasado por la caja?(R|A|T)");
            a = clsUtilidadesLP.leerCaracter();
            switch (a) {
                case 'R':
                    System.out.println("añade nombre del producto");
                    str_nombreproducto= clsUtilidadesLP.leerCadena();
                    obj_Producto = 
                    objGestor.obj_matchproducto(str_nombreproducto);
                    
                     if 
                     (objGestor.bo_hayoferta(obj_Producto))
                      {
                         dbl_precio =
                          objGestor.dbl_preciorebajado(obj_Producto);
                     }
                    objGestor.bo_eliminarproducto(obj_Producto);
                    
                    
                    
                    break;
                case 'A':
                    System.out.println("añade nombre del Accesorio");
                    str_nombreproducto= clsUtilidadesLP.leerCadena();
                    obj_Producto = 
                    objGestor.obj_matchproducto(str_nombreproducto);
                    if 
                    (objGestor.bo_hayoferta(obj_Producto))
                     {
                        dbl_precio =
                         objGestor.dbl_preciorebajado(obj_Producto);
                    }
                    objGestor.bo_eliminarproducto(obj_Producto);
                    

                    
                    break;

                default:
                    break;
            } dbl_total = dbl_total + dbl_precio;
            System.out.println("total compra: " + dbl_total);
            objGestor.vo_sumapuntoscliente(dbl_total,str_dni);

        } while (a != 'T');
        vo_menudependiente();


    }

    /**
     * Menu del trabajador con rol de almacenero
     */
    public void vo_menuAlmacenero()
    {

        SwingUtilities.invokeLater(new Runnable() {
		            @Override
		            public void run() {
		            	frmPrincipalproducto window = new 
                        frmPrincipalproducto(objGestor);
		            	window.setVisible(true);
                        
                        
		            
		            }
		        });
         int int_opcion = 99;
        
        System.out.println("Elige una opcion");
        System.out.println("1.-Nuevo producto");
        System.out.println("2.-Baja producto");
        System.out.println("3.-Modificación producto");
        System.out.println("4.-Mostrar lista productos");
        System.out.println("5.-añadir oferta");
        System.out.println("6.-salir");
        int_opcion = clsUtilidadesLP.leerEntero();
        switch (int_opcion) {
            case 1:
                System.out.println("1.-añadir prenda de ropa");
                System.out.println("2.-añadir accesorio");
                int_opcion = clsUtilidadesLP.leerEntero();
                if (int_opcion == 1)
                {
                    vo_añadirprendaropa();
                    
                } else vo_añadiraccesorio();
                break;
            case 2:
                vo_eliminarProducto(true);
                break;
            case 3:
                vo_Modificarproducto();
                break;
              case 4:
                vo_mostrarlistaproductos(1,true);
            case 5:
                vo_añadirOferta();
                break;
            case 6:
                vo_menuPrincipal();
            default:
                break;
        } 
    }
    /**
     * funcion que añade una oferta a un objeto
     */
    public void vo_añadirOferta()
    {
        System.out.println("mostrando todos los productos en catalogo");
        vo_mostrarlistaproductos(1, false);
        String nombreProducto;
        double dbl_precio;
        double dbl_oferta;
       System.out.println
       ("Introduce el nombre del producto al que quieres añadir una oferta:");
       nombreProducto = clsUtilidadesLP.leerCadena();
       System.out.println("añade porcentaje de la oferta");
        dbl_oferta= clsUtilidadesLP.leerEntero();

       
       itfProperty producto = objGestor.obj_matchproducto(nombreProducto);
       dbl_precio = (double)producto.getProperty
       (clsConstantesLN.PRECIO);
       
       if (producto.getProperty(clsConstantesLN.NOMBRE) != " ") 
       {
 
           objGestor.vo_añadirOferta(nombreProducto,dbl_precio, dbl_oferta);
           
           System.out.println
           ("Oferta \"" + dbl_oferta + "\" añadida al producto \"" +
            nombreProducto + "\"");
       } 
       else 
       {
           System.out.println("Producto con nombre \"" +
            nombreProducto + "\" no encontrado.");
       }
   }
   /**
     * Modifica cualquier producto de catalogo
     * 
     */
    public void vo_Modificarproducto()
    {   
        String str_opcion;
        vo_mostrarlistaproductos(1,false);
        vo_eliminarProducto(false);
        System.out.print("Quieres modificar el producto para que sea un "); 
        System.out.print("accesorio o para que sea una prenda? P|A");
        str_opcion = clsUtilidadesLP.leerCadena();
        switch (str_opcion)    
        {
            case "P":
                vo_añadirprendaropa();
                break;
            case "A":
                vo_añadiraccesorio();
                break;
        
            default:
                System.out.println("Valor no valido");
                break;
        }
        vo_menuAlmacenero();


    }

    /**
     * Da de alta una prenda de ropa.
     *
     */
    public void vo_añadirprendaropa()
    {
        String str_nombreprenda;
        double dbl_Precio;
        String str_talla;
        System.out.println("Introduce nombre de la prenda a añadir");
        str_nombreprenda = clsUtilidadesLP.leerCadena();
        System.out.println("Introduce precio");
        dbl_Precio = clsUtilidadesLP.leerReal();
        System.out.println("introduce talla");
        str_talla = clsUtilidadesLP.leerCadena();
        objGestor.obj_altaRopa(str_nombreprenda, dbl_Precio, str_talla,
        0);
        vo_menuAlmacenero();
    }

    /**
     * Da de alta un accesorio.
     * 
     */
    public void vo_añadiraccesorio()
    {
        String str_nombreprenda;
        double dbl_Precio;
        String str_tipo;
        double dbl_oferta = 0;
        System.out.println("Introduce nombre del accesorio a añadir");
        str_nombreprenda = clsUtilidadesLP.leerCadena();
        System.out.println("Introduce precio");
        dbl_Precio = clsUtilidadesLP.leerReal();
        System.out.println("Introduce tipo");
        str_tipo = clsUtilidadesLP.leerCadena();
       objGestor.obj_altaAccesorio(str_nombreprenda, dbl_Precio, str_tipo,
       dbl_oferta);
        vo_menuAlmacenero();
    }
/**
 * elimina un producto
 * @param bo_volver para ver si volver al menu almacenero o no
 */
    public void vo_eliminarProducto(boolean bo_volver)
    {
        if (bo_volver) 
        {
            vo_mostrarlistaproductos(1,false);
        }
        String str_nombreaeliminar;
        String str_opcion = "N";
        boolean bo_comprobacion;
        itfProperty obj_Producto;
        
        
        do 
        {
            System.out.println("escribe el nombre de la prenda a eliminar");
            str_nombreaeliminar = clsUtilidadesLP.leerCadena();
            obj_Producto = objGestor.obj_matchproducto(str_nombreaeliminar);
            bo_comprobacion = objGestor.bo_eliminarproducto(obj_Producto);
            if (!bo_comprobacion) 
            {
                System.out.println("nombre introducido no valido");
                System.out.println("¿quieres volver a intenar? S|N");
                str_opcion = clsUtilidadesLP.leerCadena();

            }
        } while (!str_opcion.equalsIgnoreCase(str_opcion));
    

       if (bo_volver) 
       {
            vo_menuAlmacenero();
       } 
        
    }
    /**
     * Funcion que muestra los productos dependiendo del filtro que se le pida
     * @param int_opcion es el filtro que se le mete
     * @param almacenero variable necesaria para que vuelva al menu de 
     * alcamenero en algun caso
     */

     public void vo_mostrarlistaproductos(int int_opcion,boolean almacenero)
     {
         ArrayList <itfProperty> arr_lista;
         TreeSet<itfProperty> tree_Productos;
         double dbl_preciorebajado = 0;
         switch (int_opcion)
          {
             case 1:
                 arr_lista= objGestor.getarrayproductos("todos");
                 System.out.println
                 ("mostrando todos los productos en catalogo");
                 for (itfProperty obj_producto : arr_lista) {
                 if (obj_producto.getProperty(clsConstantesLN.ROL).
                 equals("Prenda de ropa")) 
                 {
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.NOMBRE)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.TALLA)+ " ");
                     if(objGestor.bo_hayoferta(obj_producto))
                     {
                         dbl_preciorebajado = 
                     objGestor.dbl_preciorebajado(obj_producto);
                      System.out.print
                     (obj_producto.getProperty(clsConstantesLN.OFERTA) + "% ");
                     }
                     System.out.println
                     ("*" +obj_producto.getProperty(clsConstantesLN.ROL) + "*");
                 }
 
                 else 
                 {
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.NOMBRE)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.ROL)+ " ");
                     if(objGestor.bo_hayoferta(obj_producto))
                     {
                         dbl_preciorebajado = 
                     objGestor.dbl_preciorebajado(obj_producto);
                      System.out.print
                     (obj_producto.getProperty(clsConstantesLN.OFERTA) + "% ");
                     }
                     System.out.println
                     ("*" +obj_producto.getProperty(clsConstantesLN.ROL) + "*");
                 }}  
                 if (almacenero) {
                     vo_menuAlmacenero();
                 }
                     break;
 
             case 2:
                 arr_lista= objGestor.getarrayproductos("accesorios");
                 System.out.println
                 ("mostrando todos los accesorios en catalogo");
                 for (itfProperty obj_producto : arr_lista) 
                 {
                 System.out.print
                 (obj_producto.getProperty(clsConstantesLN.NOMBRE)+ " ");
                 System.out.print
                 (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                 System.out.println
                 (obj_producto.getProperty(clsConstantesLN.TIPO));
                 }
                 break;
             case 3:
                 arr_lista= objGestor.getarrayproductos("ropa");
                 System.out.println
                 ("mostrando todas las prendas de ropa en catalogo");
                 for (itfProperty obj_producto : arr_lista) 
                 {
                 System.out.print
                 (obj_producto.getProperty(clsConstantesLN.NOMBRE) + " ");
                 System.out.print
                 (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                 System.out.println
                 (obj_producto.getProperty(clsConstantesLN.TALLA));
                 }
                 break;
 
             case 4:
                 arr_lista= objGestor.getarrayproductos
                 ("ordenada por precio");
                 System.out.println
                 ("mostrando todos los productos en catalogo");
                 for (itfProperty obj_producto : arr_lista) {
                 if (obj_producto.getProperty(clsConstantesLN.ROL).
                 equals("Prenda de ropa")) 
                 {
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.NOMBRE)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.TALLA)+ " ");
                     System.out.println
                     ("*" +obj_producto.getProperty(clsConstantesLN.ROL) + "*");
                 }
                 else 
                 {
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.NOMBRE)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.TIPO)+ " ");
                     System.out.println
                     ("*" +obj_producto.getProperty(clsConstantesLN.ROL) + "*");
                 }}  
                     break;
                     
             
             case 5:
                 tree_Productos = objGestor.gettreesetproductos();
                 
                 System.out.println
                 ("mostrando todos los productos en catalogo");
                 for (itfProperty obj_producto : tree_Productos) {
                 if (obj_producto.getProperty(clsConstantesLN.ROL).
                 equals("Prenda de ropa")) 
                 {
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.NOMBRE)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.TALLA)+ " ");
                     System.out.println
                     ("*" +obj_producto.getProperty(clsConstantesLN.ROL) + "*");
                 }
                 else 
                 {
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.NOMBRE)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.TIPO)+ " ");
                     System.out.println
                     ("*" +obj_producto.getProperty(clsConstantesLN.ROL) + "*");
                 }}  
                     break;
                 case 6:
                 arr_lista= objGestor.getarrayproductos("todos");
                 System.out.println
                 ("mostrando todos los productos en catalogo");
                 for (itfProperty obj_producto : arr_lista) {
                 if (objGestor.bo_hayoferta(obj_producto)) 
                 {   
                 if (obj_producto.getProperty(clsConstantesLN.ROL).
                 equals("Prenda de ropa")) 
                 {
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.NOMBRE)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.TALLA)+ " ");
                     dbl_preciorebajado = 
                     objGestor.dbl_preciorebajado(obj_producto);
                      System.out.print
                     (obj_producto.getProperty(clsConstantesLN.OFERTA) + "% ");
                     System.out.print
                     ("*precio rebajado: "+dbl_preciorebajado+"* " );
                     System.out.println
                     ("*" +obj_producto.getProperty(clsConstantesLN.ROL) + "*");
                 }
 
                 else 
                 {
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.NOMBRE)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.PRECIO)+ " ");
                     System.out.print
                     (obj_producto.getProperty(clsConstantesLN.TIPO)+ " ");
                     objGestor.dbl_preciorebajado(obj_producto);
                     dbl_preciorebajado = 
                     objGestor.dbl_preciorebajado(obj_producto);
                     System.out.print
                    (obj_producto.getProperty(clsConstantesLN.PRECIO) + "% ");
                    System.out.print
                    ("*precio rebajado: "+dbl_preciorebajado+"* " );
                     System.out.println
                     ("*" +obj_producto.getProperty(clsConstantesLN.ROL) + "*");
                 }}  
                 if (almacenero) {
                     vo_menuAlmacenero();
                 }}
                 break;
                 case 7:
                 arr_lista= objGestor.getarrayproductos("todos");
                 System.out.println
                 ("mostrando todos los productos en catalogo");
                 for (itfProperty obj_producto : arr_lista) 
                 {
                     System.out.print
                     (obj_producto.getProperty
                     ("Fallo programado")+ " "); 
                 }
 
         }
       
 
 
     }

    
   /**
     * Menu de cliente que les pide DNI y contraseña para poder entrar
     */
    public void vo_MenuCliente()
    {
    
        String str_dni;
        String str_contaseña;
        itfProperty obj_cliente;
        System.out.println("Añade dni");
        str_dni = clsUtilidadesLP.leerCadena();
        System.out.println("Añade contraseña");
        str_contaseña = clsUtilidadesLP.leerCadena();
       int int_opcion;
       int_opcion= 99;
    
        if (objGestor.bo_comprobacioncliente(str_dni, str_contaseña)) 
        {
            
        
            do
            {
        System.out.print("Se le van a mostrar los productos en catalogo");
        System.out.println(" ¿como quiere verlos?");
        System.out.println("1.-Sin filtros");
        System.out.println("2.-Ver solo accesorios");
        System.out.println("3.-Ver solo prendas de ropa");
        System.out.println
        ("4.-Ver productos ordenados por precios(menor a mayor)");
        System.out.print
        ("5.-Ordenar alfabeticamente o por precio en caso de tener ");
        System.out.println("mismo nombre");
        obj_cliente = objGestor.obj_matchcliente(str_dni);
        System.out.println("6.-excepcion null pointer exception");
        if (obj_cliente.getProperty(clsConstantesLN.ROL).
                equals(en_ROL_CLIENTE.en_ROL_PREMIUM))
        {
        System.out.println
        ("7.-ver productos con oferta");
        }
        System.out.println("0.-Salir");
        
        int_opcion = clsUtilidadesLP.leerEntero();
        switch (int_opcion) {
            case 1:
                vo_mostrarlistaproductos(1,false);
                    
                break;
            case 2:
                vo_mostrarlistaproductos(2,false);
                    
                break;
            case 3:
                vo_mostrarlistaproductos(3,false);
                    
                break;
            case 4:
                vo_mostrarlistaproductos(4,false);
                    
                break;
            case 5:
                vo_mostrarlistaproductos(5,false);
                break;
            
            
            case 7:
                obj_cliente = objGestor.obj_matchcliente(str_dni);
                if 
                (obj_cliente.getProperty(clsConstantesLN.ROL).
                equals(en_ROL_CLIENTE.en_ROL_PREMIUM))
                 {
                    vo_mostrarlistaproductos(6, false);
                } else {
                    System.out.print
                    ("**Acceso denegado.");
                    System.out.println
                    ( "Solo clientes premium pueden ver ofertas**");
                }
                break;
            case 6:
                vo_mostrarlistaproductos(7, false);
            default:
                break;
            }
        }while (int_opcion != 0);
        
        } else System.out.println("**Cliente no valido**");

    }
} 

    
