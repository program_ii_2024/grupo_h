package LN;




import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Esta clase representa un producto en el sistema.
 * Proporciona métodos para obtener y establecer el nombre y el precio 
 * del producto.
 * Los empleados con roles de vista 
 * (clientes premium, clientes normales y dependientes) solo 
 * pueden ver los productos.
 * El empleado con rol de edición (almacén) puede ver y editar los productos.
 * @author Pablo
 */
public abstract class clsProducto implements itfProperty,
Comparable<clsProducto>
{
    /**
     * nombre del producto
     */
    private String str_Nombre;
    /**
     * precio del producto
     */
    private  double dbl_Precio;
    /**
     * oferta del producto
     */
    private double dbl_oferta;


    /**
     * metodo get del atributo oferta
     * @return devuelve el valor de la oferta
     */
    public double getDbl_oferta() {
        return dbl_oferta;
    }
    /**
     * añade un valor al atributo oferta
     * @param dbl_oferta es el porcentaje de la oferta a añadir
     */
    public void setDbl_oferta(double dbl_oferta) {
        this.dbl_oferta = dbl_oferta;
    }

 

    /**
     * Crea un nuevo producto con el nombre y el precio especificados.
     * @param str_Nombre El nombre del producto.
     * @param dbl_Precio El precio del producto
     */
    public clsProducto(String str_Nombre,  double dbl_Precio) 
    {
        this.str_Nombre = str_Nombre;
        this.dbl_Precio = dbl_Precio;
        this.dbl_oferta = 0;
    }

    /**
     * Obtiene el nombre del producto.
     * @return El nombre del producto.
     */
    public String getNombre() 
    {
        return str_Nombre;
    }
    /**
     * Da valor al atributo privado nombre
     * @param str_nombre nombre del producto a añadir
     */
    public void setNombre(String str_nombre)
    {
        this.str_Nombre=str_nombre;
    }

    /**
     * Obtiene el precio del producto.
     * @return El precio del producto.
     */
    public double getPrecio() 
    {
        return dbl_Precio;
        
    }

    /**
     * Da valor al atributo privado precio
     * @param dbl_Precio es el precio del producto
     */
    public void setPrecio(double dbl_Precio)
    {
        this.dbl_Precio=dbl_Precio;
    }

    /**
     * Constructor sin parametros para el objeto producto
     */
    public clsProducto()
    {
        this.str_Nombre = " ";
        this.dbl_Precio = 0;
    }
    
    /**
     * Constructor con parametros para el objeto producto
     * @param str_Nombre nombre del producto
     * @param dbl_precio precio del producto
     */
    public clsProducto(String str_Nombre,Double dbl_precio)
    {
        super();
        this.str_Nombre=str_Nombre;
        this.dbl_Precio=dbl_precio;
        
    }
    /**
     * Metodo de la interfac comparable que compara dos 
     * productos alfabeticamente y si tienen el mismo nombre por el precio mayor
     * @param arg0 objeto a comparar
     * @return devuelve un int que dice si es mayor o menor
     */
    public int compareTo(clsProducto arg0) {

        Double Dbl_estenumero = this.dbl_Precio;
        if (arg0==null) return 1;
		
		
		if (this.equals(arg0)) return 0;

        if (!this.str_Nombre.equals(arg0.getNombre()))
		{
			return this.str_Nombre.compareTo(arg0.getNombre());
		}
        return  Dbl_estenumero.compareTo(arg0.getPrecio());
    }
    
    /**
     * metodo que implementa de la interfaz itfProperty
     * @param propiedad atributo a devolver
     * @return el atributo espedificado en la propiedad
     */
    public Object getProperty(String propiedad) 
    {
        switch (propiedad) {
            case "precio":
                return this.dbl_Precio;
            case "nombre":
                return this.str_Nombre;
            case "oferta":
                return this.dbl_oferta;
            default:
            throw new clsPropertyNoExistente(propiedad);
        }
    }
    
}
