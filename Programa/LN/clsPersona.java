package LN;

import COMUN.itfProperty;
import Excepciones.clsPropertyNoExistente;

/**
 * Clase que contiene los atributos y metodos de una persona.
 * Es la clase padre de clsCliente y de clsTrabajador.
 * 
 * @author Pablo
 */
public abstract class clsPersona implements itfProperty
{
    /**
     * nombre de la persona
     */
    private String str_Nombre;
    /**
     * apellido de la persona
     */
    private String str_Apellido;
    /**
     * dni de la persona
     */
    private String str_dni;
    /**
     * contraseña de la persona
     */
    private String str_contraseña;

    /**
     * devuelve el atributo Nombre del objeto
     * 
     * @return el nombre de la persona
     */
    public String getNombre() 
    {
        return this.str_Nombre;
    }

    /**
     * Da valor al atributo privado str_Nombre
     * 
     * @param str_Nombre el nombre a añadir
     */
    public void setNombre(String str_Nombre) 
    {
        this.str_Nombre = str_Nombre;
    }

    /**
     * devuelve el atributo Apellido del objeto
     * 
     * @return el apellido de la persona
     */
    public String getApellido() 
    {
        return this.str_Apellido;
    }

    /**
     * da valor al atributo privado str_Apellido
     * 
     * @param str_Apellido apellido de la persona
     */
    public void setApellido(String str_Apellido) 
    {
        this.str_Apellido = str_Apellido;
    }

    /**
     * devuelve el atributo DNI del objeto
     * 
     * @return str_dni dni de la persona
     */
    public String getdni() 
    {
        return this.str_dni;
    }

    /**
     * da valor al atributo privado str_dni
     * 
     * @param str_dni dni de la persona a añadir
     */
    public void setdni(String str_dni) 
    {
        this.str_dni = str_dni;

    }

    /**
     * devuelve el atributo contraseña del objeto
     * 
     * @return str_contraseña
     */
    public String getcontraseña() 
    {
        return this.str_contraseña;
    }

    /**
     * Da valor al atributo privado str_contraseña
     * 
     * @param str_contraseña contraseña de la persona a añadir
     */
    public void setcontraseña(String str_contraseña) 
    {
        this.str_contraseña = str_contraseña;
    }

    /**
     * constructor con parametros
     * 
     * @param str_nombre nombre de la persona
     * @param str_Apellido apellido de la persona
     * @param str_dni dni de la persona 
     * @param str_contraseña contraseña de la persona
     */
    public clsPersona(String str_nombre, String str_Apellido, String str_dni,
    String str_contraseña) 
    {
        super();
        this.str_Nombre = str_nombre;
        this.str_Apellido = str_Apellido;
        this.str_dni = str_dni;
        this.str_contraseña = str_contraseña;
    }

    /**
     * constructor vacio que inicializa las variables
     */
    public clsPersona() 
    {
        super();
        this.str_Nombre = " ";
        this.str_Apellido = " ";
        this.str_dni = " ";
        this.str_contraseña = " ";
    }
    /**
     * metodo hashcode de la clase clsPersona
     * @return devuelve el resulatado del hasCode
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((str_dni == null) ? 0 : str_dni.hashCode());
        result = prime * result + ((str_contraseña == null) ? 0 : 
        str_contraseña.hashCode());
        return result;
    }
    /**
     * comprueba si el objeto es igual a cualquier otro
     * @param obj el objeto a comparar
     * @return si son o no iguales
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        clsPersona other = (clsPersona) obj;
        if (str_dni == null) {
            if (other.str_dni != null)
                return false;
        } else if (!str_dni.equals(other.str_dni))
            return false;
        if (str_contraseña == null) {
            if (other.str_contraseña != null)
                return false;
        } else if (!str_contraseña.equals(other.str_contraseña))
            return false;
        return true;
    }
    /**
    * metodo que recibe de la implementacion de itfproperty
    * @param propiedad la propiedad especificada
    * @return El valor actual de la propiedad especificada.
    */
    public Object getProperty(String propiedad) {
        switch(propiedad)
		{
			case "nombre": return this.str_Nombre;
            case "apellido": return this.str_Apellido;
            case "dni": return this.str_dni;
            case "contraseña" : return this.str_contraseña;
            
            default:
            throw new clsPropertyNoExistente(propiedad);
		}
        
    }
    
    
    

}