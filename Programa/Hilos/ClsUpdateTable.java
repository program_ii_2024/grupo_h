package Hilos;

import javax.swing.table.DefaultTableModel;

import COMUN.clsConstantesLN;
import COMUN.itfProperty;
import LN.clsGestorLN;
import LP.NombreRepetidoDialog;
import LP.frmPrincipalproducto;
/**
 * clase que se encarga de ir viendo cada x tiempo si la tabla se ha atualizado
 * y actualiza los objetos que se hayan actualizado en la tabla (JTable)
 */
public class ClsUpdateTable extends Thread
 {
    /**
     * modelo que se ha registrado en la anterior solicitud del modelo
     */
    private DefaultTableModel modeloAnterior;
    /**
     * modelo que se ha registrado actualmente para poder compararlo
     * con el anterior
     */
    private DefaultTableModel model;
    /**
     * formulario principal que se esta usando
     */
    private frmPrincipalproducto frm_Principalproducto;
    /**
     * objeto gestor que se esta utilizando y contiente informacion de 
     * los objetos añadidos
     */
    private clsGestorLN obj_Gestor;
    

    /**
     * constructor con parametros
     * @param frm_principal el formulario que se va a modificar
     * @param obj_Gestor el gestor al que se le van a añadir los datos
     */
    public ClsUpdateTable
    (frmPrincipalproducto frm_principal,clsGestorLN obj_Gestor)
    {
        this.frm_Principalproducto = frm_principal;
        this.obj_Gestor = obj_Gestor;
    }
    



    /**
     * funcion run de la clase, es el codigo que se ejecuta en el nuevo hilos
     * creado, su funcion es la de ir actualizando los objetos 
     * cuyos campos que se han actualizado en la tabla, tambien comprueba si los
     * datos actualizados son validos
     */
    public void run()
    {
        int int_rows = 0;
        itfProperty obj_producto;
        String str_nombre;
        int int_rowAnterior= 0;
        int p = 0;
        double precio = 0;
        double oferta = 0;
        String str_nombreOtro;
        String str_nombreAnterior;
        boolean valorRepetido = false;
        modeloAnterior = frm_Principalproducto.getTableModel();
        while (true) 
        {
            model = frm_Principalproducto.getTableModel();
            int_rows = model.getRowCount();
            if(p != 0){
            
            if (int_rows < int_rowAnterior)
            {
                vo_eliminarEliminado();
            }
            if (int_rows> int_rowAnterior)
            {
                vo_añadirAñadido();
            }
            for (int fila = 0; fila < int_rows; fila++) 
            {
                for (int columna = 0; columna < model.getColumnCount(); 
                columna++) {
                    Object valorTablaActual = model.getValueAt(fila, columna);
                    Object valorTablaAnterior = modeloAnterior.getValueAt
                    (fila, columna);
                   
                    if (!valorTablaActual.equals(valorTablaAnterior)) {
                    if (columna == 0) {
                        for (int i = 0; i < model.getRowCount(); i++) {
                            str_nombreOtro = (String) 
                            model.getValueAt(i, 0);
                            str_nombre = (String) valorTablaActual;
                            if(str_nombre.equals(str_nombreOtro)&&  fila != i)
                            {
                                valorRepetido = true;
                            }
                        }
                    }
                       
                        if (valorRepetido)
                        {
                            NombreRepetidoDialog dialog = 
                            new NombreRepetidoDialog(this,frm_Principalproducto);
                            dialog.setVisible(true);
                            str_nombreAnterior = (String) 
                        modeloAnterior.getValueAt(fila, 0);
                            model.setValueAt(str_nombreAnterior, fila, 0);
                        } 
                        else 
                        {
                        str_nombreAnterior = (String) 
                        modeloAnterior.getValueAt(fila, 0);
                        obj_producto = obj_Gestor.obj_matchproducto
                        (str_nombreAnterior);
                        obj_Gestor.bo_eliminarproducto(obj_producto);
                        String str_precio = String.valueOf
                        (model.getValueAt(fila, 1));
                        String ofertaStr = String.valueOf
                        (model.getValueAt(fila, 3));
                        try {
                            precio = Double.parseDouble(str_precio);
                            oferta = Double.parseDouble(ofertaStr);
                        } catch (NumberFormatException e) {
                            System.err.println
                        ("(DEBUG)Error: Los valores no son numéricos en lafila " 
                            + fila);
                        }

                        if(obj_producto.getProperty(clsConstantesLN.ROL).equals
                        ("Accesorio")) {
                       
                        obj_Gestor.obj_altaAccesorio(
                        (String)model.getValueAt(fila, 0),
                         precio, 
                        (String)model.getValueAt(fila, 2),
                        oferta);}
                        else  obj_Gestor.obj_altaRopa(
                            (String)model.getValueAt(fila, 0),
                             precio, 
                            (String)model.getValueAt(fila, 2),
                            oferta);
                        System.out.println("(DEBUG) objeto añadido");
                    }}
                }
            }
        }
            p++;
            modeloAnterior = clonarModelo(model);

            int_rowAnterior = model.getRowCount();
            try {
				this.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        
    }
    }
    /**
     * funcion que le da el valor del modelo actual al modelo anterior antes
     * de volver a leer el modelo actual
     * @param modelo el modelo actual a clonar
     * @return el modelo anterior clonado al actual
     */
    public DefaultTableModel clonarModelo(DefaultTableModel modelo) {
        DefaultTableModel clon = new DefaultTableModel();
        for (int i = 0; i < modelo.getColumnCount(); i++) {
            clon.addColumn(modelo.getColumnName(i));
        }
        for (int i = 0; i < modelo.getRowCount(); i++) {
            Object[] fila = new Object[modelo.getColumnCount()];
            for (int j = 0; j < modelo.getColumnCount(); j++) {
                fila[j] = modelo.getValueAt(i, j);
            }
            clon.addRow(fila);
        }
        return clon;
    }
    /**
     * al eliminar una fila en la tabla el run da una excepcion por que las
     * tablas no tienen la misma longitud por ello antes de nada se elimina 
     * en el modelo anterior la fila eliminada, esto se hace con esta funcion
     */
    public void vo_eliminarEliminado()
    {
        int row = modeloAnterior.getRowCount();
        for (int i = 0; i < model.getRowCount(); i++) {
            
            Object valorTablaActual = model.getValueAt(i, 0);
            Object valorTablaAnterior = modeloAnterior.getValueAt(i, 0);
            if (valorTablaActual != valorTablaAnterior)
            {
                modeloAnterior.removeRow(i);
                return;
            }
        }
        modeloAnterior.removeRow(row-1);
    }
    
    /**
     * al añadir una fila en la tabla el run da una excepcion por que las
     * tablas no tienen la misma longitud por ello antes de nada se añade 
     * en el modelo anterior la fila añadida, esto se hace con esta funcion
     */
    public void vo_añadirAñadido()
    {
        int int_row = model.getRowCount();
        int_row = int_row -1;
        modeloAnterior.addRow(new Object[]{model.getValueAt(int_row,0),
            model.getValueAt(int_row,1),
            model.getValueAt(int_row,2),
            model.getValueAt(int_row,3)});
    }
}


