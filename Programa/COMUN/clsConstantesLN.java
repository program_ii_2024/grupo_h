package COMUN;

/**
 * clase comun que guarda todos los diferentes roles que pueden tener
 * las diferentes clases(cliente, producto, trabajador...)
 * @author Juan
 */
public class clsConstantesLN 
{
    /**
     *guarda los roles de los clientes
     */
    public enum en_ROL_CLIENTE
    {
        /**
         * rol de cliente premium
         */
        en_ROL_PREMIUM,
        /**
         * rol de socio normal
         */
        en_ROL_SOCIONORMAL,
        /**
         * rol de socio invalido
         */
        en_ROL_INVALIDO;

        
       
    }

     /**
     *guarda los roles de los trabajadores
     */
    public enum en_ROL_TRABAJADOR
    {
        /**
         * rol de almacenero
         */
        en_ROL_ALMACENERO,
        /**
         * rol de dependiente
         */
        en_ROL_DEPENDIENTE,
        /**
         * rol invalido
         */
        en_ROL_INVALIDO

    }
    /**
     * constante que se le pasa al metodo get proprety para pedir el precio
     */
    public static final String PRECIO = "precio";
    /**
     * constante que se le pasa al metodo get proprety para pedir el nombre
     */
    public static final String NOMBRE = "nombre";
    /**
     * constante que se le pasa al metodo get proprety para pedir la oferta
     */
    public static final String OFERTA = "oferta";
    /**
     * constante que se le pasa al metodo get proprety para pedir el rol
     */
    public static final String ROL = "quesoy";
    /**
     * constante que se le pasa al metodo get proprety para pedir el apellido
     */
    public static final String APELLIDO = "apellido";
    /**
     * constante que se le pasa al metodo get proprety para pedir el dni
     */
    public static final String DNI = "dni";
    /**
     * constante que se le pasa al metodo get proprety para pedir la contraseña
     */
    public static final String CONTRASEÑA = "contraseña";
    /**
     * constante que se le pasa al metodo get proprety para pedir el Numero de 
     * socio
     */
    public static final String NUM_SOCIO = "Numero socio";
    /**
     * constante que se le pasa al metodo get proprety para pedir los 
     * puntos de socio
     */
    public static final String PUNTOS_DE_SOCIO = "puntos de socio";
    /**
     * constante que se le pasa al metodo get proprety para pedir la talla
     */
    public static final String TALLA = "talla";
    /**
     * constante que se le pasa al metodo get proprety para pedir el tipo de
     * accesorio
     */
    public static final String TIPO = "tipo";
    /**
     * constante que se le pasa al metodo get proprety para pedir el NNSS
     */
    public static final String NNSS = "NNSS";


}
