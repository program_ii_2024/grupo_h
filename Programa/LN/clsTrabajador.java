package LN;



import COMUN.clsConstantesLN.en_ROL_TRABAJADOR;

/**
 * Clase que contiene los atributos de un trabajador. 
 * Es la clase hija de clsPersona.
 * @author Pablo
 */
public class clsTrabajador extends clsPersona
{
    /**
     * numero de la seguridad social del trabajador
     */
    private int int_NNSS;
    /**
     * rol del trabajador
     */
    private en_ROL_TRABAJADOR en_Rol_TRABAJADOR;
    
    /**
     * devuelve el atributo NNSS del objeto
     * @return int_NNSS
     */
    public int getNNSS() 
    {
        return this.int_NNSS;
    }

    /**
     * Da valor al atributo privado NNSS
     * @param int_NNSS numero a añadir
     */
    public void setNNSS(int int_NNSS) 
    {
        this.int_NNSS = int_NNSS;
    }

    /**
     * Devuelve el rol del trabajador
     * @return en_Roltrabajador
     */
    public en_ROL_TRABAJADOR getRolTrabajador()
    {
        return en_Rol_TRABAJADOR;
    }

    /**
     * Da valor al rol del cliente
     * @param en_Roltrabajador el rol a añadir
     */
    public void setRolcliente(en_ROL_TRABAJADOR en_Roltrabajador) 
    {
        this.en_Rol_TRABAJADOR = en_Roltrabajador;
    }

    /**
     * Constructor con parametros de los objetos trabajador
     * @param str_nombre nombre del tarabajador
     * @param str_Apellido apellido del trabajador
     * @param str_dni dni del trabajador
     * @param str_contraseña contraseña del trabajador
     * @param int_NNSS numero de la seguridad social del trabajador
     * @param en_roltrabajador rol del trabajador
     */
    public clsTrabajador(String str_nombre, String str_Apellido, 
    String str_dni, String str_contraseña, int int_NNSS, 
    en_ROL_TRABAJADOR en_roltrabajador) 
    {
        super(str_nombre, str_Apellido, str_dni, str_contraseña);
        this.int_NNSS = int_NNSS;
        this.en_Rol_TRABAJADOR=en_roltrabajador;
    }

    /**
     * constructor sin parametros del objeto trabajador
     */
    public clsTrabajador()
    {
        super();
        this.int_NNSS = 00000000000;
        this.en_Rol_TRABAJADOR=en_ROL_TRABAJADOR.en_ROL_INVALIDO;
    }
    /**
     * metodo que implementa de itfproperty de la clase trabajador
     * @param propiedad el atributo a devolver
     * @return el atributo especificado en la propiedad
     */
    @Override
    public Object getProperty(String propiedad) {
        switch(propiedad)
		{
			
            case "NNSS": return this.int_NNSS;
            case "quesoy" : if 
            (this.en_Rol_TRABAJADOR == en_ROL_TRABAJADOR.en_ROL_ALMACENERO) 
            {
                return "almacenero";  
            } else return "dependiente";
            default:
            return super.getProperty(propiedad);
		}
		
       
    }

    


    
}