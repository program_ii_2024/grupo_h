


import LP.clsMenu;


/**
 * Es la funcion main que llama al primer menu.
 * @author Ander
 */
public class clsMain 
{
    /**
     * funcion main que inicializa el programa
     * @param args argumentos
     */
    public static void main(String[] args) 
    {
		
        clsMenu obj_Menu;   
        obj_Menu = new clsMenu();

        obj_Menu.vo_comienzomenu();
        
    }
}
