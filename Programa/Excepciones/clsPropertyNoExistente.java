package Excepciones;

/**
 * Excepcion de tipo implicita que se lanzara cuando se solicite, a traves 
 * de la interfaz itfProperty, cualquier
 * propiedad que no contenga el objeto destino.
 * Se trata de un excepcion implicita, por lo que no hay que tratarla. 
 * Si salta es poque hay un error de programacion.
 * @author Juan
 *
 */

public class clsPropertyNoExistente extends RuntimeException 
{
	/**
	 * Propiedad solicitada y que no existe en el objeto correspondiente.
	 */
	private String prop;
	
	/**
	 * constructor de la clase clsPropertyNoExistente
	 * @param propiedad es la propiedad erronea que es ha enviado
	 */
	public clsPropertyNoExistente(String propiedad)
	{
		this.prop= propiedad;
		
	}
	/**
	 * mensaje que devuelve en caso de saltar la excepcion
	 */
	@Override
	public String getMessage() 
	{
	
		return "La propiedad solicitada "+ "*" + this.prop + "*" 
        + " no existe en el objeto.";
	}
	
	
	

}
