package COMUN;
/**
 * clase de constante DB
 * @author Juan
 */
public class clsConstantesDB {
    /**
	 * columna del DNI
	 */
    public static final String TABLA_CLIENTE_DNI = "Dni";
	/**
	 * columna del Nombre del cliente
	 */
    public static final String TABLA_CLIENTE_NOMBRE = "Nombre";
		/**
	 * columna del Nombre del producto
	 */
    public static final String TABLA_PRODUCTO_NOMBRE = "nombre";
	
	/**
	 * columna del precio del producto
	 */
    public static final String TABLA_PRODUCTO_PRECIO = "Precio";
		/**
	 * columna de la talla o tipo del producto
	 */
    public static final String TABLA_PRODUCTO_TALLA_TIPO = "Nombre";

		/**
	 * columna de la oferta del producto
	 */
    public static final String TABLA_PRODUCTO_OFERTA = "oferta";
	 /**
	 * columna del Apellido
	 */
    public static final String TABLA_CLIENTE_APELLIDO = "Apellido";
	/**
	 * columna de la contraseña
	 */
    public static final String TABLA_CLIENTE_CONTRASEÑA = "Contraseña";
	/**
	 * columna del Numero de socio
	 */
    public static final String TABLA_CLIENTE_NUM_SOCIO = "Numero_de_Socio";
	/**
	 * columna de los puntos de socio
	 */
	public static final String TABLA_CLIENTE_PUNTOS_SOCIO = "puntos_de_socio";
	/**
	 * columna del rol del cliente
	 */
	public static final String TABLA_CLIENTE_ROL = "en_rol_cliente";

	/**
	 * columna del rol del cliente
	 */
	public static final String TABLA_PRODUCTO_ROL = "en_rol_Producto";

	
	

	/**
	 * comando para insertar en mysql un nuevo cliente
	 */
	public static final String SQL_INSERT_CLIENTE =
	"INSERT INTO `mydb`.`Cliente` " + 
		"(`Dni`,`Nombre`,`Apellido`,`Contraseña`," +
		"`Numero_de_Socio`,`puntos_de_socio`,`en_rol_cliente`) " +
		"VALUES(?, ?, ?, ?, ?, ?, ?);"; 

		/**
	 * comando para insertar en mysql un nuevo producto
	 */
	public static final String SQL_INSERT_PRODUCTO =
	"INSERT INTO `mydb`.`Producto` " + 
		"(`nombre`,`precio`,`talla_tipo`,`oferta`,`en_rol_Producto`)" +
		"VALUES(?, ?, ?, ?, ?);"; 
	

	/**
	 * comando para seleccionar todos los atributos de un cliente
	 */
	public static final String SQL_SELECT_CLIENTE =
	"SELECT* from Cliente;";
	/**
	 * comando para seleccionar todos los atributos de un Producto
	 */
	public static final String SQL_SELECT_PRODUCTO =
	"SELECT* from `mydb`.`Producto`;";
   
	/**
	 * comando para eliminar una tupla de la tabla cliente en mysql
	 */
    public static final String SQL_DELETE_CLIENTE_BY_DNI= 
	"DELETE FROM " +
        "`cliente`\r\n" + "where Dni= ?;";

		/**
	 * comando para eliminar una tupla de la tabla producto en mysql
	 */
    public static final String SQL_DELETE_PRODUCTO_BY_NOMBRE= 
	"DELETE FROM " +
        "`Producto`\r\n" + "where Nombre= ?;";
	/**
	 * comando para modificar los puntos
	 * de socio una tupla de la tabla cliente en mysql
	 */
	public static final String SQL_UPDATE_CLIENTE_BY_NUMERO_SOCIO=
		"Update `mydb`.`Cliente` set `puntos_de_socio` = ? " + 
		"WHERE `Dni` = ?;";

	/**
	 * comando para modificar el rol de una tupla de la tabla cliente en mysql
	 */
	public static final String SQL_UPDATE_CLIENTE_ROL_BY_NUMERO_SOCIO=
		"Update `mydb`.`Cliente` set `en_rol_cliente` = ? " + 
		"WHERE `Dni` = ?;";
	

	
}
