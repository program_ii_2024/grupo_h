package Excepciones;

/**
 * Excepcion a lanzar en caso de que se introduzca un 
 * alumno repetido en el sistema.
 * Se trata de una excepcion explicita y por lo tanto se debe tratar.
 * @author Juan
 *
 */
public class clsPersonaRepetidaExcepcion extends Exception 
{
	/**
	 * dni de la persona repetida
	 */
	private String str_dni;
	/**
	 * la contraseña de la persona repetida
	 */
	private String str_contraseña;
	
	/**
	 * Constructor de la excepcion en la que se indican el dni y nombre 
     * reptidos.
	 * @param str_dni dni repetido
	 * @param str_contraseña contraseña de la persona
	 */
	public clsPersonaRepetidaExcepcion(String str_dni, String str_contraseña)
	{
		
		this.str_dni = str_dni;
		this.str_contraseña=str_contraseña;
	}
	/**
	 * mensaje que devuelve en caso de saltar la excepcion
	 */
	@Override
	public String getMessage() 
	{
		return "La persona con dni " + this.str_dni + " y contraseña " + 
        this.str_contraseña + " esta repetido en el sistema. No sera añadido.";
	}
	
	

}

