package LN;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

import COMUN.clsConstantesLN;
import COMUN.itfProperty;
import COMUN.clsConstantesLN.en_ROL_CLIENTE;
import COMUN.clsConstantesLN.en_ROL_TRABAJADOR;

import static COMUN.clsConstantesDB.TABLA_CLIENTE_APELLIDO;
import static COMUN.clsConstantesDB.TABLA_CLIENTE_CONTRASEÑA;
import static COMUN.clsConstantesDB.TABLA_CLIENTE_DNI;
import static COMUN.clsConstantesDB.TABLA_CLIENTE_NOMBRE;
import static COMUN.clsConstantesDB.TABLA_CLIENTE_NUM_SOCIO;
import static COMUN.clsConstantesDB.TABLA_CLIENTE_PUNTOS_SOCIO;
import static COMUN.clsConstantesDB.TABLA_CLIENTE_ROL;
import static COMUN.clsConstantesDB.TABLA_PRODUCTO_NOMBRE;
import static COMUN.clsConstantesDB.TABLA_PRODUCTO_OFERTA;
import static COMUN.clsConstantesDB.TABLA_PRODUCTO_PRECIO;
import static COMUN.clsConstantesDB.TABLA_PRODUCTO_ROL;
import static COMUN.clsConstantesDB.TABLA_PRODUCTO_TALLA_TIPO;

import Excepciones.clsPersonaRepetidaExcepcion;
import LD.clsGestorDatos;

/**
 * Clase que gestiona la informacion perteneciente a la logica de negocio y
 * sirve de intermediario entre esta capa y las demas capas
 * @author Juan
 */
public class clsGestorLN 
{
    
    
    /**
     * array de los trabajadores
     */
    ArrayList<clsTrabajador> arr_trabajadores; 
    /**
     * array que contiene a los productos
     */
    ArrayList<clsProducto> arr_Productos;
    /**
     * hashmap de trabajadores
     */
    private HashMap <String, clsTrabajador> map_listaTrabajador;
    /**
     * hashet de trabajadores
     */
    private HashSet<clsTrabajador>set_listaTrabajadores;
    /**
     * array de clientes que contiene los clientes
     */
    ArrayList<clsCliente> arr_Clientes;
    /**
     * hasmap de clientes
     */
    private HashMap<String, clsCliente> map_listaclientes;
    /**
     * hashset de clientes
     */
    private HashSet<clsCliente>set_listaclientes;
    /**
     * obj gestor de datos
     */
    private clsGestorDatos obj_datos;
    

    /**
     * inicializa los arrays
     */
    public clsGestorLN() 
    {
        arr_trabajadores = new ArrayList<clsTrabajador>();
        arr_Productos = new ArrayList<clsProducto>();
        map_listaTrabajador = new HashMap<String, clsTrabajador>();
		set_listaTrabajadores= new HashSet<clsTrabajador>();
        arr_Clientes = new ArrayList<clsCliente>();
        map_listaclientes = new HashMap<String, clsCliente>();
		set_listaclientes= new HashSet<clsCliente>();
        obj_datos=new clsGestorDatos();
    }
       /**
     * Comprueba si el dni y contraseña metidos concuerdan con algn
     * ya registrado
     * @param str_dni dni del trabajador
     * @param str_contraseña contraseña del trabajador
     * @return devuelve un boolean de si es correcto o no
     */
    public boolean bo_ComprobacionTrabajador(String str_dni, String 
    str_contraseña)
    {
        for (clsTrabajador obj_Trabajador : arr_trabajadores) {
            if (obj_Trabajador.getdni().equals(str_dni) && 
            obj_Trabajador.getcontraseña().equals(str_contraseña)) {
                return true;
                
            }
        }
        
        return false;
    }

    /** 
    * Pasa los parametros al constructor de trabajador y mete el objeto en el 
    * array de trabajadores.
    * @param str_nombre es el nombre a meter en el objtrabajador
    * @param str_Apellido es el apellido a meter en el objtrabajador
    * @param str_dni es el dni a meter en el objtrabajador
    * @param str_contraseña es la contraseña a meter en el objtrabajador
    * @param int_NNSS es el numero de la seguridad social a meteren el 
    * objtrabajador
    * @param en_roltrabajador es el rol a meter en el objtrabajador
    * @return objTrabajador es el objeto en si
    * @throws clsPersonaRepetidaExcepcion excepcion de una persona repetida
    */
    public itfProperty vo_altaTrabajador(String str_nombre, 
    String str_Apellido, String str_dni, String str_contraseña, int int_NNSS,
    en_ROL_TRABAJADOR en_roltrabajador) throws clsPersonaRepetidaExcepcion
    {
        clsTrabajador obj_Trabajador;
        obj_Trabajador = new clsTrabajador(str_nombre, str_Apellido, str_dni,
        str_contraseña, int_NNSS, en_roltrabajador);
        obj_Trabajador.setNombre(str_nombre);
        obj_Trabajador.setApellido(str_Apellido);
        obj_Trabajador.setcontraseña(str_contraseña);
        obj_Trabajador.setNNSS(int_NNSS);
        obj_Trabajador.setRolcliente(en_roltrabajador);
        if (set_listaTrabajadores.add(obj_Trabajador)) {
            arr_trabajadores.add(obj_Trabajador);
            map_listaTrabajador.put(str_dni, obj_Trabajador);
            return obj_Trabajador;
        }
        else
        {
            throw new clsPersonaRepetidaExcepcion(str_dni, str_contraseña);
        }
       

    }

    /**
     * devuelve el array de trabajadores
     * 
     * @return arr_trabajadores
     */
    public ArrayList<itfProperty> getarraytrabajadores() 
    {
        ArrayList<itfProperty> listaAux;
		
		listaAux = new ArrayList<itfProperty>();
		for(clsTrabajador obj_Trabajador: arr_trabajadores)
			listaAux.add(obj_Trabajador);
			
		return listaAux;
    }

    /**
     * devuelve el array de productos dependiendo del tipo que se le pida,
     * tambien devuelve array ordenados por precio
     * @param tipo es el tipo de array que se quiere devolver muy parecido a un
     * filtro
     * @return devuelve un array de tipo itfproperty de los productos que se
     * hayan solicitado
     */
    public ArrayList<itfProperty> getarrayproductos(String tipo) 
    {
       
        ArrayList<clsProducto> arr_productosordenados;
        arr_productosordenados = new ArrayList<clsProducto>();
        ArrayList<itfProperty> listaAux;
		listaAux = new ArrayList<itfProperty>();
		switch (tipo) {
            case "todos":
            
		for(clsProducto obj_Producto: arr_Productos)
			listaAux.add(obj_Producto);
			
		return listaAux;
                
            case "ropa":
            for(clsProducto obj_Producto: arr_Productos)
            {
                if (obj_Producto instanceof clsRopa) 
                {
                    listaAux.add(obj_Producto);
                } 
               
                
            }
            return listaAux;
            case "accesorios" :
            for(clsProducto obj_Producto: arr_Productos)
            {
                if (obj_Producto instanceof clsAccesorio) 
                {
                    listaAux.add(obj_Producto);
                } 
            }  
            return listaAux;
            

            case "ordenada por precio":
            for (clsProducto obj_Producto : arr_Productos)
            {
              arr_productosordenados.add(obj_Producto);
            }
            
            Collections.sort(arr_productosordenados,new clsComparadorprecios());
            for (clsProducto obj_Producto : arr_productosordenados) 
            {
                listaAux.add(obj_Producto);
            }
            return listaAux;
            default: return listaAux;
        }


    }

    /**
     * Encuentra a un tarbajador de los ya registrados con lo parametros
     * introducidos
     * @param str_dni es el dni de la persona a encontrar
     * @return obj_trabajadormatch es el objeto del tabajador
     */
    public itfProperty obj_matchTrabajador(String str_dni) 
    {
        itfProperty obj_trabajadormatch;
        obj_trabajadormatch = new clsTrabajador();
        obj_trabajadormatch = map_listaTrabajador.get(str_dni);
        return obj_trabajadormatch;
    }

    /**
     * Da de alta una prenda de ropa
     * 
     * @param str_nombre nombre de la prenda
     * @param dbl_Precio precio de la prenda
     * @param str_talla talla de la prenda
     * @param dbl_oferta oferta de la prenda
     * @return devuelve el objeto creado de tipo clsproducto
     */
    public clsRopa obj_altaRopa(String str_nombre, double dbl_Precio,
    String str_talla,double dbl_oferta) 
    {
        clsRopa obj_Ropa;
        obj_Ropa = new clsRopa(str_nombre, dbl_Precio, str_talla);
        obj_Ropa.setNombre(str_nombre);
        obj_Ropa.setPrecio(dbl_Precio);
        obj_Ropa.setTalla(str_talla);
        obj_Ropa.setDbl_oferta(dbl_oferta);
        arr_Productos.add(obj_Ropa);
        obj_datos.vo_connect();
        obj_datos.int_insertarproducto(str_nombre, 
        dbl_Precio, str_talla, 2, dbl_oferta);
        obj_datos.vo_disconnect();

        return obj_Ropa;

    }
    /**
     * metodo que da de baja un cliente
     * @param str_Dni el numero de socio del cliente
     */

    public void vo_bajacliente(String str_Dni)
    {
        clsCliente obj_cliente;
        obj_cliente = new clsCliente();
        obj_cliente = map_listaclientes.get(str_Dni);
        arr_Clientes.remove(obj_cliente);
        map_listaclientes.remove(str_Dni,obj_cliente);
        set_listaclientes.remove(obj_cliente);
		obj_datos.vo_connect();
        obj_datos.int_eliminarCliente(str_Dni);
		obj_datos.vo_disconnect();
        
    }
    /**
     * Añade un objeto clsaccesorio al array y le da valores al objeto como tal
     * 
     * @param str_nombre nombre del accesorio
     * @param dbl_Precio precio del accesorio
     * @param str_tipo tipo del accesorio
     * @param oferta oferta del accesorio
     * @return devuelve el propio accesorio creado
     */
    public clsAccesorio obj_altaAccesorio(String str_nombre, double dbl_Precio,
    String str_tipo, double oferta) 
    {
        clsAccesorio obj_Accesorio;
        obj_Accesorio = new clsAccesorio(str_nombre, dbl_Precio, str_tipo);
        obj_Accesorio.setNombre(str_nombre);
        obj_Accesorio.setPrecio(dbl_Precio);
        obj_Accesorio.setTipo(str_tipo);
        obj_Accesorio.setDbl_oferta(oferta);
        arr_Productos.add(obj_Accesorio);
        obj_datos.vo_connect();
        obj_datos.int_insertarproducto(str_nombre, 
        dbl_Precio, str_tipo, 2, oferta);
        obj_datos.vo_disconnect();
        return obj_Accesorio;

    }

    /**
     * Elimina un producto independientemente de que tipo sea
     * 
     * @param obj_producto obj del tipo itproperty del producto a eliminar
     * @return devuelve un boolean de si la operación ha sio satisfactoria
     */
    public boolean bo_eliminarproducto(itfProperty obj_producto) 
    {
        String str_nombre;
        if (!obj_producto.getProperty("nombre").equals(" ")) {
            arr_Productos.remove(obj_producto);
            obj_datos.vo_connect();
            str_nombre = (String)
             obj_producto.getProperty(clsConstantesLN.NOMBRE);
        obj_datos.int_eliminarProducto(str_nombre);
		obj_datos.vo_disconnect();
            return true;
        }

        return false;
    }

    /**
     * encuentra un producto en el array en base a un nombre que se le da
     * 
     * @param str_nombre nombre del producto
     * @return devuelve el producto en si
     */
    public itfProperty obj_matchproducto(String str_nombre) 
    {

        itfProperty obj_accesoriovacio;
        obj_accesoriovacio = new clsAccesorio();
        itfProperty objProducto;
        objProducto = new clsAccesorio();

        for (clsProducto objProductomatch : arr_Productos) 
        {
            if (objProductomatch.getNombre().equals(str_nombre)) 
            {

                objProducto = objProductomatch;
                return objProducto;
            }
        }
        return obj_accesoriovacio;
    }
    /**
     * modifica un producto eliminando el que se ha pedido y poniendo otro en su
     * lugar
     * @param str_Nombreamodificar nombre de la lista a modificar
     * @param str_nombre nuevo nombre a añadir
     * @param dbl_Precio precio del producto
     * @param str_tipo tipo en caso de ser un accesorio
     * @param str_talla talla en caso de ser una prenda
     * @param dbl_oferta oferta de la persona a añadir
     */
    public void vo_Modificarproducto(String str_Nombreamodificar,
    String str_nombre, double dbl_Precio,
    String str_tipo,String str_talla,double dbl_oferta)
    {
        
        bo_eliminarproducto(obj_matchproducto(str_Nombreamodificar));
        if (str_talla.equals("no")) {
            obj_altaAccesorio(str_nombre, dbl_Precio, str_tipo,dbl_oferta);
            
        }else obj_altaRopa(str_nombre, dbl_Precio, str_talla,dbl_oferta);


    }
    /**
     * funcion que inicializa un objeto del tipo clscliente, lo añade al array
     * de clientes dando una excepcion si no entra en el hasmap
     * @param str_nombre nombre de la persona a añadir
     * @param str_apellido apellido de la persona a añadir
     * @param str_DNI dni de la persona a añadir
     * @param str_contraseña contraseña de la persona a añadir
     * @param int_numSocio numero de socio de la persona a añadir
     * @return el objeto creado pero de tipo itfproperty
     * @throws clsPersonaRepetidaExcepcion excepcion si la persona añadida
     * se considera repetida
     */
    public itfProperty obj_nuevoCliente(String str_nombre,String str_apellido, 
    String str_DNI, String str_contraseña, int int_numSocio )
    throws clsPersonaRepetidaExcepcion
     {
        int id;
        clsCliente obj_Cliente;
        obj_Cliente = new clsCliente(str_nombre, str_apellido, str_DNI,
        str_contraseña,  en_ROL_CLIENTE.en_ROL_SOCIONORMAL,int_numSocio);
        obj_Cliente.setNombre(str_nombre);
        obj_Cliente.setApellido(str_apellido);
        obj_Cliente.setcontraseña(str_contraseña);
        obj_Cliente.setNumsocio(int_numSocio);
        obj_Cliente.setRolcliente(en_ROL_CLIENTE.en_ROL_SOCIONORMAL);
        if (set_listaclientes.add(obj_Cliente)) {
            arr_Clientes.add(obj_Cliente);
            map_listaclientes.put(str_DNI, obj_Cliente);
            obj_datos.vo_connect();
            id = obj_datos.int_insertarcliente(str_nombre, 
            str_apellido, str_DNI, str_contraseña, int_numSocio,
            en_ROL_CLIENTE.en_ROL_SOCIONORMAL);
            System.out.println("DEBUG:Nuevo id: " + id);
            obj_datos.vo_disconnect();
        }
        else 
        {
            throw  new clsPersonaRepetidaExcepcion(str_DNI,str_contraseña);
        }
        
        return obj_Cliente;

        
    }
    /**
     * Comprueba si el numero de socio y la contraseña son validos en el login
     * de clietne
     * @param str_dni numero de socio de la persona a encontrar
     * @param str_contraseña contraseña de la persona a encontrar
     * @return devuelve si son correctos o no
     */
    public boolean 
    bo_comprobacioncliente(String str_dni,String str_contraseña)
    {
        ArrayList<itfProperty> arr_Clientes;
        arr_Clientes = ListarCLientesBD();
        for (itfProperty obj_cliente : arr_Clientes) {
            String prueba = (String)obj_cliente.getProperty(clsConstantesLN.DNI);
            if (prueba.equals(str_dni) && 
            obj_cliente.getProperty("contraseña").
            equals(str_contraseña)) {
                return true;
                
            }
        }
        return false;
            
    }

  /**
   * Suma puntos de socio a un cliente
   * @param dbl_total numero de puntos a añadir
   * @param str_Dni dni la persona a añadir
   */
    public void vo_sumapuntoscliente(Double dbl_total, String str_Dni)
    {
        clsCliente obj_cliente;
        int int_puntosañadidos;
        int_puntosañadidos =(int) (dbl_total * 0.25);
        obj_cliente = obj_matchcliente(str_Dni);
        obj_cliente.set_puntosdesocio(int_puntosañadidos);
		obj_datos.vo_connect();
		obj_datos.int_insertarpuntosdesocio
        (int_puntosañadidos,str_Dni);
		obj_datos.vo_disconnect();
        if (obj_cliente.get_puntosdesocio() >= 100) {
            obj_cliente.setRolcliente(en_ROL_CLIENTE.en_ROL_PREMIUM); 
            obj_datos.vo_connect();
		obj_datos.int_cambiararolpremium(str_Dni);
		obj_datos.vo_disconnect();

        }
        
    }

    /**
     * encuentra un socio
     * @param str_Dni numero de socio de la persona a encontrar
     * @return el objeto clsCliente que contiene la informacion del cliente
     * encontrado
     */
    public clsCliente obj_matchcliente(String str_Dni) 
    {
        clsCliente obj_clientematch;
        obj_clientematch = new clsCliente();
        obj_clientematch = map_listaclientes.get(str_Dni);
        return obj_clientematch;
    }
    /**
     * devuelve el treeset de productos ordenados
     * @return treeset de productos ordenadors
     */
    public TreeSet<itfProperty> gettreesetproductos()
    {
        TreeSet<itfProperty> tree_Productos;
        tree_Productos = new TreeSet<itfProperty>();
        for (clsProducto objProducto : arr_Productos)
        {
           tree_Productos.add(objProducto);
        }
        return tree_Productos;
    }
    /**
     * funcion practica que inicializa unos objetos ejemplo para hacer mas
     * rapida la visualizacion de si todo va como debe ir o no
     * @throws clsPersonaRepetidaExcepcion excepcion de persona repetida
     */
    public void vo_inicializacionobjetos() throws clsPersonaRepetidaExcepcion
    {
       
        vo_altaTrabajador
        ("prueba", "prueba"
        , "almacenero", 
        "almacenero", 
        0, en_ROL_TRABAJADOR.en_ROL_ALMACENERO);

        
        vo_altaTrabajador
        ("prueba", "prueba"
        , "dependiente", 
        "dependiente", 
        0, en_ROL_TRABAJADOR.en_ROL_DEPENDIENTE);
        
       

        
        vo_altaTrabajador
        ("prueba", "prueba"
        , "almacenero", 
        "almacenero", 
        0, en_ROL_TRABAJADOR.en_ROL_ALMACENERO);

    }
    /**
     * añade una oferta en un producto
     * @param str_nombreProducto el nombre del producto
     * @param dbl_precio el precio
     * @param dbl_oferta la oferta
     */
    public void vo_añadirOferta
    (String str_nombreProducto,double dbl_precio, double dbl_oferta)
    {
        
        for (clsProducto objProducto : arr_Productos) 
        {
            if (objProducto.getNombre().equals(str_nombreProducto) ||
            objProducto.getPrecio() == dbl_precio) 
            {
                objProducto.setDbl_oferta(dbl_oferta);
            }
        }
    }
    /**
     * funcion que te devuelve el precio rebajado
     * @param obj_Producto producto al que hay que sacarle el precio
     * rebajado
     * @return el precio rebajado
     */
    public double dbl_preciorebajado(itfProperty obj_Producto)
    {
        double dbl_precio;
        double dbl_preciorebajado;
        double dbl_oferta;
        dbl_precio = (double)
        obj_Producto.getProperty("precio");
        dbl_oferta = (double) 
         obj_Producto.getProperty("oferta");
        dbl_preciorebajado = dbl_precio * (dbl_oferta/100); 
        dbl_precio = dbl_precio-dbl_preciorebajado;
        return dbl_precio;
    }
    /**
     * funcion que te dice si hay oferta o no hay oferta
     * @param obj_producto producto del que hay que comprobar si hay 
     * oferta
     * @return hay oferta o no
     */
    public boolean bo_hayoferta(itfProperty obj_producto)
    {
        if ((double) obj_producto.getProperty("oferta") == 0) 
        {
         return false;   
        }else return true;
    }
   

    
/**
 * funcion que sirve para listar los trabajadores de la BD
 * @return devuelve el array list de los trabajadores de la BD
 */
public ArrayList<itfProperty> ListarCLientesBD ()
	{
		ResultSet rs_clientes;
		ArrayList<itfProperty> retorno;
		
			
		this.obj_datos.vo_connect();
		rs_clientes = this.obj_datos.rs_listadoClientes();
		retorno = new ArrayList<itfProperty>();
		if(rs_clientes != null)
		{
			try
			{
				while(rs_clientes.next())
				{
					clsCliente aux = new clsCliente();
					aux.setNombre(rs_clientes.
                    getString(TABLA_CLIENTE_NOMBRE));
					aux.setApellido(rs_clientes.
                    getString(TABLA_CLIENTE_APELLIDO));
					aux.setdni(rs_clientes.
                    getString(TABLA_CLIENTE_DNI));
					aux.setcontraseña(rs_clientes.
                    getString(TABLA_CLIENTE_CONTRASEÑA));
					aux.setNumsocio(rs_clientes.getInt
                    (TABLA_CLIENTE_NUM_SOCIO));
                    aux.set_puntosdesocio(rs_clientes.getInt
                    (TABLA_CLIENTE_PUNTOS_SOCIO));
                    String str_rol = rs_clientes.getString(TABLA_CLIENTE_ROL);
                    switch (str_rol) {
                        case "en_ROL_PREMIUM":
                                aux.setRolcliente
                                (en_ROL_CLIENTE.en_ROL_PREMIUM);
                                break;
                        case "en_ROL_SOCIONORMAL":
                                aux.setRolcliente
                                (en_ROL_CLIENTE.en_ROL_SOCIONORMAL);
                                break;
                        case "en_ROL_INVALIDO":
                                aux.setRolcliente
                                (en_ROL_CLIENTE.en_ROL_INVALIDO);
                                break;
                            
                    
                        default:
                            break;
                    }
                    

					retorno.add(aux);
				}
			}
			catch(SQLException e)
			{
				System.out.println("DEBUG: Error en rs_cliente: " + e);
			}

		}
		
				
		this.obj_datos.vo_disconnect();
		return retorno;
	}

    /**
     * funcion que sirve para meter todos los clientes de la base de datos en 
     * el array propio del gestor
     */
    public void arr_inicializararrays()
    {
        ArrayList<itfProperty> arr_clientes;
        arr_clientes = ListarCLientesBD();
        for (itfProperty objCliente : arr_clientes) 
        {
            if(set_listaclientes.add((clsCliente) objCliente))
            {
            arr_Clientes.add((clsCliente) objCliente);
            map_listaclientes.put((String)objCliente.getProperty
            (clsConstantesLN.DNI),(clsCliente)objCliente);
            }

        }

        ArrayList<itfProperty> arr_productos;
        arr_productos = ListarProductosBD();
        for (itfProperty objProducto : arr_productos) 
        {
           
            arr_Productos.add((clsProducto) objProducto);
            

        }
    }
   

    /**
 * funcion que sirve para listar los trabajadores de la BD
 * @return devuelve el array list de los trabajadores de la BD
 */
public ArrayList<itfProperty> ListarProductosBD ()
{
    ResultSet rs_Productos;
    ArrayList<itfProperty> retorno;
    
        
    this.obj_datos.vo_connect();
    rs_Productos = this.obj_datos.rs_listadoProductos();
    retorno = new ArrayList<itfProperty>();
    if(rs_Productos != null)
    {
        try
        {
            while(rs_Productos.next())
            {
                String str_rol = rs_Productos.getString(TABLA_PRODUCTO_ROL);
                if (str_rol.equals
                ("en_ROL_Prenda")) 
                {
                    clsRopa aux = new clsRopa();
                    aux.setNombre(rs_Productos.
                    getString(TABLA_PRODUCTO_NOMBRE));
                    aux.setPrecio(rs_Productos.
                    getDouble(TABLA_PRODUCTO_PRECIO));
                    aux.setTalla(rs_Productos.
                    getString(TABLA_PRODUCTO_TALLA_TIPO));
                    aux.setDbl_oferta(rs_Productos.getDouble
                    (TABLA_PRODUCTO_OFERTA));
                    retorno.add(aux);

                } else 
                {
                    clsAccesorio aux = new clsAccesorio();
                    aux.setNombre(rs_Productos.
                    getString(TABLA_PRODUCTO_NOMBRE));
                    aux.setPrecio(rs_Productos.
                    getDouble(TABLA_PRODUCTO_PRECIO));
                    aux.setTipo(rs_Productos.
                    getString(TABLA_PRODUCTO_TALLA_TIPO));
                    aux.setDbl_oferta(rs_Productos.getDouble
                    (TABLA_PRODUCTO_OFERTA)); 
                    retorno.add(aux);

                }
                
               
                }
                

                
                
            }
        catch(SQLException e)
        {
            System.out.println("DEBUG: Error en rs_producto: " + e);
        }

    }
    
            
    this.obj_datos.vo_disconnect();
    return retorno;
}
   

}