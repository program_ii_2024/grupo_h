package LD;

import static COMUN.clsConstantesDB.SQL_DELETE_CLIENTE_BY_DNI;
import static COMUN.clsConstantesDB.SQL_DELETE_PRODUCTO_BY_NOMBRE;
import static COMUN.clsConstantesDB.SQL_INSERT_CLIENTE;
import static COMUN.clsConstantesDB.SQL_INSERT_PRODUCTO;
import static COMUN.clsConstantesDB.SQL_SELECT_CLIENTE;
import static COMUN.clsConstantesDB.SQL_SELECT_PRODUCTO;
import static COMUN.clsConstantesDB.SQL_UPDATE_CLIENTE_BY_NUMERO_SOCIO;
import static COMUN.clsConstantesDB.SQL_UPDATE_CLIENTE_ROL_BY_NUMERO_SOCIO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import COMUN.clsConstantesLN.en_ROL_CLIENTE;


/**
 * Clase encargada de la interacción con la base de datos.
 */
public class clsGestorDatos 
{
	
    private static final String URL="jdbc:mysql://localhost:3306/";
	private static final String SCHEMA = "mydb";
    private static final String PARAMS="?useUnicode=true" + 
                                       "&useJDBCCompliantTimezoneShift=true"+
                                       "&useLegacyDatetimeCode=false"+
                                       "&serverTimezone=UTC&useSSL=false";
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String USER = "root";
	private static final String PASS = "root";

    /**
	 * Objeto para crear la conexi�n a base de datos.
	 */
	private Connection obj_conn;
	
	/**
	 * Objeto para crear la consulta a base de datos.
	 */
	private PreparedStatement obj_ps;
	
	

    /**
     * Constructor de la clase que inicializa los atributos a valores por
     * defecto.
     */
    public clsGestorDatos()
    {
        obj_conn = null;
        obj_ps = null;
    
    }

    /**
	 * Metodo para la conexion a la base de datos.
	 */
	 public void vo_connect()
	 {
		 try 
		 {
			//Se carga el driver de acceso a datos
		   Class.forName(DRIVER);
		   obj_conn = DriverManager.getConnection(URL+SCHEMA+PARAMS,USER,PASS);
		   System.out.println("Debug: Connected to the database");  
		 }
		 catch (Exception e) 
		 {
		     System.out.println("Debug: NO CONNECTION ");
		 }
	 }
	 
	 /**
      * Metodo para la deconexion de la base de datos.
      */
	 public void vo_disconnect()
	 {
		 
		 try 
		 {
			obj_conn.close();
			obj_ps.close(); // cerrar el statement tb cierra el resultset.
		 } 
		 catch (SQLException e) 
		 {
			/*
             * Si hay algún problema en la desconexion, no hago nada.
             * Se intenta de nuevo en el bloque finally.
            */
		 }
		 finally 
		 {
			 try {obj_conn.close();} catch(Exception e){/*no hago nada*/}
			 try {obj_ps.close();} catch(Exception e){/*no hago nada*/}
		 }
	 }
    /**
	 * funcion que inserta un cliente en la BD
	 * @param str_nombre nombre del cliente
	 * @param str_apellido apellido del cliente
	 * @param str_DNI dni del cliente
	 * @param str_contraseña contraseña del cliente
	 * @param int_numSocio numero de socio del cliente
	 * @param en_RolNuevo rol nuevo del cliente
	 * @return int para ver si ha ido bien o no
	 */
     public int int_insertarcliente(String str_nombre, 
	 String str_apellido, String str_DNI, String str_contraseña, 
	 int int_numSocio, en_ROL_CLIENTE en_RolNuevo)
	 {
		 int regActualizados=0;
		 int retorno=0;
		 int rol = 3;
		switch (en_RolNuevo) {
			case en_ROL_SOCIONORMAL:
				rol = 2;
				break;
			case en_ROL_PREMIUM:
				rol = 1;
				break;
			case en_ROL_INVALIDO:
				rol = 3;
				break;
		
			default:
				break;

		}
         
	
		 try 
		 {
			this.obj_ps = this.obj_conn.prepareStatement(SQL_INSERT_CLIENTE,
                PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setString(2, str_nombre);
			this.obj_ps.setString(3, str_apellido);
			this.obj_ps.setString(1, str_DNI);
			this.obj_ps.setString(4, str_contraseña);
            this.obj_ps.setInt(5, int_numSocio);
			this.obj_ps.setInt(6, 0);
			this.obj_ps.setInt(7, rol);

			
			regActualizados=this.obj_ps.executeUpdate();
			
			if(regActualizados ==1)
			{
				ResultSet rs = this.obj_ps.getGeneratedKeys();
	            if(rs.next())
	            {
	                retorno= rs.getInt(1);
	            }
			}
		 } 
		 catch (SQLException e) 
		 {
			System.out.println("DEBUG: Error al introducir un alumno " + 
                "en la base de datos: " + SQL_INSERT_CLIENTE);
		 }
		 return retorno;
	 }

	 public int int_insertarproducto(String str_nombre, 
	 Double dbl_precio,String str_talla_tipo, int int_rol, Double dbl_oferta)
	 {
		 int regActualizados=0;
		 int retorno=0;
		 
		
         
	
		 try 
		 {
			this.obj_ps = this.obj_conn.prepareStatement(SQL_INSERT_PRODUCTO,
                PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setString(1, str_nombre);
			this.obj_ps.setDouble(2, dbl_precio);
			this.obj_ps.setString(3, str_talla_tipo);
			this.obj_ps.setDouble(4, dbl_oferta);
			this.obj_ps.setInt(5, int_rol);

			
			regActualizados=this.obj_ps.executeUpdate();
			
			if(regActualizados ==1)
			{
				ResultSet rs = this.obj_ps.getGeneratedKeys();
	            if(rs.next())
	            {
	                retorno= rs.getInt(1);
	            }
			}
		 } 
		 catch (SQLException e) 
		 {
			System.out.println("DEBUG: Error al introducir un alumno " + 
                "en la base de datos: " + SQL_INSERT_PRODUCTO);
		 }
		 return retorno;
	 }


     /**
      * Metodo que da de baja 
      * @param str_Dni el numero de socio de la persona a dar de baja
      * @return un numero para ver si ha salido bien
      */
    public int int_eliminarCliente(String str_Dni)
     {
        int regActualizados=0;
		int retorno=0;
        
		try 
		{
			this.obj_ps = this.obj_conn.prepareStatement(
                                    SQL_DELETE_CLIENTE_BY_DNI);
			this.obj_ps.setString(1, str_Dni);			
			regActualizados=this.obj_ps.executeUpdate();
			retorno = regActualizados;
		 } 
		 catch (SQLException e) 
		 {
			System.out.println("DEBUG: Error al introducir un alumno " + 
                "en la base de datos: " + SQL_DELETE_CLIENTE_BY_DNI);
		 }
		 return retorno;
     }

	     /**
      * Metodo que da de baja 
      * @param str_Dni el numero de socio de la persona a dar de baja
      * @return un numero para ver si ha salido bien
      */
	  public int int_eliminarProducto(String str_nombre)
	  {
		 int regActualizados=0;
		 int retorno=0;
		 
		 try 
		 {
			 this.obj_ps = this.obj_conn.prepareStatement(
									 SQL_DELETE_PRODUCTO_BY_NOMBRE);
			 this.obj_ps.setString(1, str_nombre);			
			 regActualizados=this.obj_ps.executeUpdate();
			 retorno = regActualizados;
		  } 
		  catch (SQLException e) 
		  {
			 System.out.println("DEBUG: Error al introducir un alumno " + 
				 "en la base de datos: " + SQL_DELETE_PRODUCTO_BY_NOMBRE);
		  }
		  return retorno;
	  }
    /**
	 * funcion que devuelve un listado de todos los clientes
	 * @return el listado de los clientes
	 */
    public ResultSet rs_listadoClientes() 
    {
        ResultSet rs_retorno = null;

        try 
			{
				this.obj_ps = this.obj_conn.prepareStatement(
                                                            SQL_SELECT_CLIENTE);
				rs_retorno=this.obj_ps.executeQuery();
			} 
			catch (SQLException e) 
			{
				System.out.println("Error en la SQL= " + 
                                    SQL_SELECT_CLIENTE + " " + e);
			}

        return rs_retorno;
    }
	    /**
	 * funcion que devuelve un listado de todos los clientes
	 * @return el listado de los clientes
	 */
    public ResultSet rs_listadoProductos() 
    {
        ResultSet rs_retorno = null;

        try 
			{
				this.obj_ps = this.obj_conn.prepareStatement(
                                                            SQL_SELECT_PRODUCTO);
				rs_retorno=this.obj_ps.executeQuery();
			} 
			catch (SQLException e) 
			{
				System.out.println("Error en la SQL= " + 
                                    SQL_SELECT_PRODUCTO + " " + e);
			}

        return rs_retorno;
    }
	/**
	 * funcion que añade puntos de socio a un cliente
	 * @param int_puntosdesocio son los puntos a añadir
	 * @param str_Dni dni del socio al que hay que añadirle los puntos
	 * @return numero de control para ver si ha ido bien
	 */
	public int int_insertarpuntosdesocio(int int_puntosdesocio, String str_Dni)
	{
		int regActualizados=0;
		int retorno=0;
		
   
		try 
		{
		   this.obj_ps = this.obj_conn.prepareStatement
		   (SQL_UPDATE_CLIENTE_BY_NUMERO_SOCIO,
			   PreparedStatement.RETURN_GENERATED_KEYS);
		   this.obj_ps.setString(2, str_Dni);
		   this.obj_ps.setInt(1, int_puntosdesocio);

		   
		   regActualizados=this.obj_ps.executeUpdate();
		   
		   if(regActualizados ==1)
		   {
			   ResultSet rs = this.obj_ps.getGeneratedKeys();
			   if(rs.next())
			   {
				   retorno= rs.getInt(1);
			   }
		   }
		} 
		catch (SQLException e) 
		{
		   System.out.println("DEBUG: Error al introducir un alumno " + 
			   "en la base de datos: " + SQL_UPDATE_CLIENTE_BY_NUMERO_SOCIO);
		}
		return retorno;
	}
	/**
	 * funcion que cambia el rol a premium si se ha llegado a 100 puntos
	 * @param str_Dni el socio al que hay que añadirle los puntos
	 * @return numero de control para ver si ha ido bien
	 */
	public int int_cambiararolpremium(String str_Dni)
	{
		int regActualizados=0;
		int retorno=0;
		
   
		try 
		{
		   this.obj_ps = this.obj_conn.prepareStatement
		   (SQL_UPDATE_CLIENTE_ROL_BY_NUMERO_SOCIO,
			   PreparedStatement.RETURN_GENERATED_KEYS);
		   this.obj_ps.setString(2, str_Dni);
		   this.obj_ps.setInt(1, 1);

		   
		   regActualizados=this.obj_ps.executeUpdate();
		   
		   if(regActualizados ==1)
		   {
			   ResultSet rs = this.obj_ps.getGeneratedKeys();
			   if(rs.next())
			   {
				   retorno= rs.getInt(1);
			   }
		   }
		} 
		catch (SQLException e) 
		{
		   System.out.println("DEBUG: Error al introducir un alumno " + 
			   "en la base de datos: " + SQL_UPDATE_CLIENTE_BY_NUMERO_SOCIO);
		}
		return retorno;
	}
    }
