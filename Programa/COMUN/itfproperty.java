package COMUN;
/**
 * interfaz itfProperty que sirve para aislar las capas LN y LP
 * @author Juan
 */
public interface itfProperty
{
	/**
	 * metodo a añadir por las clases que la implementen
	 * @param propiedad la propiedad que llega para saber que devolver
	 * @return devuelve un atributo del objeto
	 */
	public Object getProperty (String propiedad);

}
