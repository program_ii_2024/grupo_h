package LP;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import Hilos.ClsUpdateTable;
import java.awt.GridLayout;

/**
 * clase que lanza un jdialog en el caso de que se intente añadir un pruducto
 * con el nombre de otro ya existente
 * @author Juan
 */
public class NombreRepetidoDialog extends JDialog {
    
    /**
     * primer constructor del Jdialog
     * @param frmPrincipalproducto el frm que se esta usando para saber donde
     * lanzar el dialog
     */
    public NombreRepetidoDialog(frmPrincipalproducto frmPrincipalproducto) {
        
        super(frmPrincipalproducto, "Nombre Repetido", true); 
        // Crear un panel para contener la etiqueta con la imagen y el texto
        JPanel panel = new JPanel(new GridLayout(2, 1));
        
        // Cargar la imagen desde un archivo
        ImageIcon icono = new ImageIcon("Imagenes/5220262.png");
        
        JLabel imagenLabel = new JLabel(icono);
        imagenLabel.setIcon
        (new ImageIcon(icono.getImage().getScaledInstance
        (100, 100, java.awt.Image.SCALE_SMOOTH)));
        
        
        panel.add(imagenLabel);
        
        
        JLabel label = new JLabel("El nombre ya existe o es nulo.");
        
        
        panel.add(label);
        
       
        getContentPane().add(panel);
        
        
        pack();
        
        setLocationRelativeTo(frmPrincipalproducto);
        
        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
    }
    /**
     * segundo constructor
     * @param clsUpdateTable el objeto hilo
     * @param frm_Principalproducto el frm principal para saber donde lanzar el
     * jdialog
     */
    public NombreRepetidoDialog(ClsUpdateTable clsUpdateTable,
    frmPrincipalproducto frm_Principalproducto) {
        
        super(frm_Principalproducto, "Nombre Repetido", true); 
        
        // Crear un panel para contener la etiqueta con la imagen y el texto
        JPanel panel = new JPanel(new GridLayout(2, 1));
        
        // Cargar la imagen desde un archivo
        ImageIcon icono = new ImageIcon("Imagenes/5220262.png");
        
        JLabel imagenLabel = new JLabel(icono);
        imagenLabel.setIcon
        (new ImageIcon(icono.getImage().getScaledInstance
        (100, 100, java.awt.Image.SCALE_SMOOTH)));
        
        
        panel.add(imagenLabel);
        
        
        JLabel label = new JLabel("El nombre ya existe o es nulo.");
        
        
        panel.add(label);
        
       
        getContentPane().add(panel);
        
        
        pack();
        setLocationRelativeTo(frm_Principalproducto);
    
        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
    }

    
}
